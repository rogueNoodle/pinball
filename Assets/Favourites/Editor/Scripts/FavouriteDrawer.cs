﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Roguenoodle.Favourites
{
    public static class FavouriteDrawer 
    {

        private static double lastClickTime = 0;
        private static Color cachedColor = Color.white;


        public static bool Draw (IFavourite fav)
        {

            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.BeginVertical(Styles.UtilityButtonGroup);
                {
                    cachedColor = GUI.backgroundColor;
                    GUI.backgroundColor = Styles.RemoveButtonColor;
                    if (GUILayout.Button(Styles.RemoveButtonContent, Styles.UtilityButton))
                    {
                        fav.ShouldRemove = true;
                        FavouritesWindow.RequestRemoval();
                    }
                    GUI.backgroundColor = cachedColor;
                }
                EditorGUILayout.EndVertical();

                bool drawIcon = false;
                if (EditorGUIUtility.currentViewWidth > Styles.FavouriteHeight * 4)
                {
                    drawIcon = true;
                }
                Rect favRect = EditorGUILayout.BeginHorizontal(Styles.FavContainer);
                {
                    if (GUI.Button(favRect, Styles.PrimaryButtonContent, Styles.FavButtonBase))
                    {
                        if (EditorApplication.timeSinceStartup - lastClickTime < 0.25f)
                        {
                            fav.SecondaryAction();
                        }
                        else
                        {
                            fav.MainAction();
                        }
                        lastClickTime = EditorApplication.timeSinceStartup;
                    }
                    if (drawIcon)
                    {
                        GUILayout.Label(fav.MainIcon, Styles.MainIcon);
                    }

                    Styles.MainLabel.fixedWidth = favRect.width - (drawIcon ? Styles.FavouriteHeight : 0) - 8;
                    Styles.SecondaryLabel.fixedWidth = favRect.width - (drawIcon ? Styles.FavouriteHeight : 0) - 8;

                    EditorGUILayout.BeginVertical();
                    {
                        GUILayout.Label(fav.MainLabel, Styles.MainLabel);
                        GUILayout.Label(fav.SecondaryLabel + ((fav.Extension == null || fav.Extension == string.Empty) ?  "" : "   |   " + fav.Extension), Styles.SecondaryLabel);
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndHorizontal();

                if (fav.HasExtras)
                {
                    cachedColor = GUI.contentColor;
                    GUI.contentColor = fav.ShouldDrawExtras ? Styles.AccentColor : Color.grey;  
                    if (GUILayout.Button(fav.ToggleExtrasContent, Styles.ActionButton))
                    {
                        fav.ShouldDrawExtras = !fav.ShouldDrawExtras;
                    }
                    GUI.contentColor = cachedColor;
                }

                cachedColor = GUI.contentColor;
                GUI.contentColor = Styles.AccentColor;
               
                if (GUILayout.Button(fav.SecondaryActionContent, Styles.ActionButton))
                {
                    fav.SecondaryAction();
                }
                GUI.contentColor = cachedColor;

            }
            EditorGUILayout.EndHorizontal();    
            if (fav.ShouldDrawExtras)
            {
                fav.DrawExtras ();
            }
            return true;
        }
    }
}

