﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Roguenoodle.Favourites
{
    public static class Styles 
    {

        public static float FavouriteHeight
        {
            get { return 42; }
        }

        private static Color accentColorPro = Preferences.GetColorWithKey("AccentColorPro");
        public static Color AccentColorPro 
        {
            get
            {
                return accentColorPro;
            }
            set
            {
                accentColorPro = value;
                UpdateStyleColors (accentColorPro);
            }
        }

        private static void UpdateStyleColors (Color color)
        {
            ToolbarPopup.normal.textColor = color;
            MainLabel.normal.textColor = color;
            HierarchyItem.active.textColor = color;
            FavouritesWindow.RepaintWindow();
        }

        private static Color accentColorPE = Preferences.GetColorWithKey("AccentColorPE");
        public static Color AccentColorPE 
        {
            get
            {
                return accentColorPE;
            }
            set
            {
                accentColorPE = value;
                UpdateStyleColors (accentColorPE);
            }
        }


        public static Color AccentColor
        {
            get
            {
                return EditorGUIUtility.isProSkin ? accentColorPro : accentColorPE;
            }
        }
            
        private static Color removeButtonColorPro = new Color (0.25f,0.25f,0.25f);
        private static Color removeButtonColorPE = new Color (0.66f, 0.66f, 0.66f);
        public static Color RemoveButtonColor
        {
            get
            {
                return EditorGUIUtility.isProSkin ? removeButtonColorPro : removeButtonColorPE;
            }
        }

        private static Color hierarchyItemTintPro = new Color(0.82f,0.82f,0.82f,1f);
        private static Color HierarchyItemTintPE = new Color(0.95f,0.95f,0.95f,1f);
        public static Color HierarchyItemTint
        {
            get
            {
                return EditorGUIUtility.isProSkin ? hierarchyItemTintPro : HierarchyItemTintPE;
            }
        }

        private static GUIStyle toolbar = null;
        public static GUIStyle Toolbar
        {
            get
            {
                if (toolbar == null)
                {
                    toolbar = new GUIStyle(GUI.skin.FindStyle("toolbar"));
                    toolbar.alignment = TextAnchor.LowerCenter;
                    toolbar.padding = new RectOffset(3,5,0,2);
                }
                return toolbar;
            }
        }

        private static GUIStyle toolbarLabel = null;
        public static GUIStyle ToolbarLabel
        {
            get
            {
                if (toolbarLabel == null)
                {
                    toolbarLabel = new GUIStyle(Toolbar);
                }
                return toolbarLabel;    
            }
        }

        private static GUIStyle toolbarPopup = null;
        public static GUIStyle ToolbarPopup
        {
            get
            {
                if (toolbarPopup == null)
                {
                    toolbarPopup = new GUIStyle(GUI.skin.FindStyle("toolbarPopup"));
                    toolbarPopup.fontStyle = FontStyle.Italic;
                    toolbarPopup.normal.textColor = Styles.AccentColor;
                    toolbarPopup.fontStyle = FontStyle.Bold;
                }
                return toolbarPopup;
            }
        }
        private static GUIStyle sortToolbarPopup = null;
        public static GUIStyle SortToolbarPopup
        {
            get
            {
                if (sortToolbarPopup == null)
                {
                    sortToolbarPopup = new GUIStyle(ToolbarPopup);
                }
                return sortToolbarPopup;
            }
        }

        private static GUIStyle toolbarButton = null;
        public static GUIStyle ToolbarButton
        {
            get
            {
                if (toolbarButton == null)
                {
                    toolbarButton = new GUIStyle(EditorStyles.toolbarButton);
                    toolbarButton.fontStyle = FontStyle.Bold;
                    toolbarButton.fixedWidth = 37;
                }
                return toolbarButton;
            }
        }

        private static GUIStyle pinDivider = null;
        public static GUIStyle PinDivider
        {
            get
            {
                if (pinDivider == null)
                {
                    pinDivider = new GUIStyle(GUIStyle.none);
                    pinDivider.normal.background = WhiteBlock;
                    pinDivider.fixedHeight = 3f;
                    pinDivider.border = new RectOffset(1,1,1,1);
                }
                return pinDivider;
            }
        }

        private static GUIStyle favButtonBase = null;
        public static GUIStyle FavButtonBase
        {
            get
            {
                if (favButtonBase == null)
                {
                    favButtonBase = new GUIStyle(GUIStyle.none);
                    favButtonBase.normal.background = ButtonBackgroundNormal;
                    favButtonBase.active.background = ButtonBackgroundActive;
                    favButtonBase.border = new RectOffset(3,3,3,3);
                }
                return favButtonBase;
            }

        }

        private static GUIStyle favContainer = null;
        public static GUIStyle FavContainer
        {
            get
            {
                if (favContainer == null)
                {
                    favContainer = new GUIStyle(FavButtonBase);
                    favContainer.fixedHeight = FavouriteHeight;
                    favContainer.margin = new RectOffset(0,-1,0,0);
                }
                return favContainer;
            }
        }

        private static GUIStyle mainIcon = null;
        public static GUIStyle MainIcon
        {
            get
            {
                if (mainIcon == null)
                {
                    mainIcon = new GUIStyle(GUIStyle.none);
                    mainIcon.fixedHeight = FavouriteHeight;
                    mainIcon.fixedWidth = FavouriteHeight;
                    mainIcon.padding = new RectOffset(8,3,3,3);
                    mainIcon.alignment = TextAnchor.MiddleCenter;
                }
                return mainIcon;
            }
        }

        private static GUIStyle mainLabel = null;
        public static GUIStyle MainLabel
        {
            get
            {
                if (mainLabel == null)
                {
                    mainLabel = new GUIStyle(EditorStyles.largeLabel);
                    mainLabel.fontStyle = FontStyle.Bold;
                    mainLabel.normal.textColor = AccentColor;
                    mainLabel.clipping = TextClipping.Clip;
                }
                return mainLabel;
            }
        }

        private static GUIStyle secondaryLabel = null;
        public static GUIStyle SecondaryLabel
        {
            get
            {
                if (secondaryLabel == null)
                {
                    secondaryLabel = new GUIStyle(EditorStyles.miniLabel);
                    secondaryLabel.alignment = TextAnchor.UpperLeft;
                    secondaryLabel.padding = new RectOffset(2,0,-4,0);
                    secondaryLabel.clipping = TextClipping.Clip;

                }
                return secondaryLabel;
            }
        }

        private static GUIStyle utilityButtonGroup = null;
        public static GUIStyle UtilityButtonGroup
        {
            get
            {
                if (utilityButtonGroup == null)
                {
                    utilityButtonGroup = new GUIStyle(GUIStyle.none);
                    utilityButtonGroup.fixedWidth = 24;
                    utilityButtonGroup.fixedHeight = FavouriteHeight;
                    utilityButtonGroup.fixedHeight = FavouriteHeight;
                    utilityButtonGroup.margin = new RectOffset(1,0,0,0);
                }
                return utilityButtonGroup;
            }
        }

        private static GUIStyle utilityButton = null;
        public static GUIStyle UtilityButton
        {
            get
            {
                if (utilityButton == null)
                {
                    utilityButton = new GUIStyle(GUIStyle.none);
                    utilityButton.alignment = TextAnchor.MiddleCenter;
                    utilityButton.margin = new RectOffset(0,0,10,0);
                    utilityButton.fixedHeight = 21;
                    utilityButton.fixedWidth = 24;
                    utilityButton.normal.background = RemoveIcon;
                    utilityButton.active.background = RemoveIconActive;
                }
                return utilityButton;
            }
        }

        private static GUIStyle actionButton = null;
        public static GUIStyle ActionButton
        {
            get
            {
                if (actionButton == null)
                {
                    actionButton = new GUIStyle(FavButtonBase);
                    actionButton.alignment = TextAnchor.MiddleCenter;
                    actionButton.fixedHeight = FavouriteHeight;
                    actionButton.fixedWidth = actionButton.fixedHeight;
                    actionButton.margin = new RectOffset(0,0,0,0);
                }
                return actionButton;
            }
        }

        private static GUIStyle hierarchyItemContainer = null;
        public static GUIStyle HierarchyItemContainer
        {
            get
            {
                if (hierarchyItemContainer == null)
                {
                    hierarchyItemContainer = new GUIStyle(UtilityButtonGroup);
                    hierarchyItemContainer.padding = new RectOffset();
                    hierarchyItemContainer.margin = new RectOffset();
                    hierarchyItemContainer.fixedHeight = FavouriteHeight * 0.75f;
                }
                return hierarchyItemContainer;
            }
        }

        private static GUIStyle hierarchyItem = null;
        public static GUIStyle HierarchyItem
        {
            get
            {
                if (hierarchyItem == null)
                {
                    hierarchyItem= new GUIStyle(EditorStyles.label);
                    hierarchyItem.fixedHeight = (int)(FavouriteHeight * 0.75f);
                    hierarchyItem.padding = new RectOffset(3,3,3,3);
                    hierarchyItem.margin = new RectOffset(0,0,0,0);
                    hierarchyItem.alignment = TextAnchor.MiddleLeft;
                    hierarchyItem.fontStyle = FontStyle.Italic;
                    hierarchyItem.active.textColor = AccentColor;
                    hierarchyItem.clipping = TextClipping.Clip;
                }
                return hierarchyItem;
            }
        }

        private static GUIStyle hierarchyIndicator = null;
        public static GUIStyle HierarchyIndicator
        {
            get
            {
                if (hierarchyIndicator == null)
                {
                    hierarchyIndicator= new GUIStyle(GUIStyle.none);
                    hierarchyIndicator.fixedHeight = (int)(FavouriteHeight * 0.75f);
                    hierarchyIndicator.fixedWidth = 2f;

                    hierarchyIndicator.margin = new RectOffset(1,6,0,0);
                    hierarchyIndicator.alignment = TextAnchor.MiddleLeft;
                    hierarchyIndicator.normal.background = WhiteBlock;
                }
                return hierarchyIndicator;
            }
        }

        private static GUIStyle scrollView = null;
        public static GUIStyle ScrollView
        {
            get
            {
                if (scrollView == null)
                {
                    scrollView= new GUIStyle(GUIStyle.none);
                    scrollView.normal.background = UtilBackground;
                }
                return scrollView;
            }
        }

        private static GUIStyle dropBox = null;
        public static GUIStyle DropBox
        {
            get
            {
                if (dropBox == null)
                {
                    dropBox= new GUIStyle(GUIStyle.none);
                    dropBox.normal.background = DropBoxOutline;
                    dropBox.border = new RectOffset (2,2,2,2);
                    dropBox.margin = new RectOffset (1,0,0,1);
                }
                return dropBox;
            }
        }

        private static Texture2D dropBoxOutline = null;
        public static Texture2D DropBoxOutline
        {
            get
            {
                if (dropBoxOutline == null)
                {
                    dropBoxOutline = Resources.Load("favourites_dropbox_outline") as Texture2D;
                }
                return dropBoxOutline;
            }
        }

        private static Texture2D buttonBackgroundNormal = null;
        public static Texture2D ButtonBackgroundNormal
        {
            get
            {
                if (buttonBackgroundNormal == null)
                {
                    buttonBackgroundNormal = 
                        EditorGUIUtility.isProSkin ? 
                        Resources.Load("favourites_bg_normal_pro") as Texture2D :
                        Resources.Load("favourites_bg_normal_pe") as Texture2D;
                }
                return buttonBackgroundNormal;
            }
        }

        private static Texture2D utilBackground = null;
        public static Texture2D UtilBackground
        {
            get
            {
                if (utilBackground == null)
                {
                    utilBackground = 
                        EditorGUIUtility.isProSkin ? 
                        Resources.Load("favourites_util_bg_pro") as Texture2D :
                        Resources.Load("favourites_util_bg_pe") as Texture2D;
                }
                return utilBackground;
            }
        }


        private static Texture2D buttonBackgroundActive = null;
        public static Texture2D ButtonBackgroundActive
        {
            get
            {
                if (buttonBackgroundActive == null)
                {
                    buttonBackgroundActive = 
                        EditorGUIUtility.isProSkin ? 
                        Resources.Load("favourites_bg_active_pro") as Texture2D :
                        Resources.Load("favourites_bg_active_pe") as Texture2D;
                }
                return buttonBackgroundActive;
            }
        }

        private static Texture2D whiteBlock = null;
        public static Texture2D WhiteBlock
        {
            get
            {
                if (whiteBlock == null)
                {
                    whiteBlock = Resources.Load("favourites_white_block") as Texture2D;
                }
                return whiteBlock;
            }
        }

        private static Texture starIcon = null;
        public static Texture StarIcon
        {
            get
            {
                if (starIcon == null)
                {
                    starIcon = Resources.Load("favourites_icon") as Texture;
                }
                return starIcon;
            }
        }

        private static Texture pinIcon = null;
        public static Texture PinIcon
        {
            get
            {
                if (pinIcon == null)
                {
                    pinIcon = Resources.Load("favourites_pin") as Texture;
                }
                return pinIcon;
            }
        }

        private static Texture2D removeIcon = null;
        public static Texture2D RemoveIcon
        {
            get
            {
                if (removeIcon == null)
                {
                    removeIcon = Resources.Load("favourites_remove") as Texture2D;
                }
                return removeIcon;
            }
        }

        private static Texture2D removeIconActive = null;
        public static Texture2D RemoveIconActive
        {
            get
            {
                if (removeIconActive == null)
                {
                    removeIconActive = Resources.Load("favourites_remove-active") as Texture2D;
                }
                return removeIconActive;
            }
        }

        private static Texture openIcon = null;
        public static Texture OpenIcon
        {
            get
            {
                if (openIcon == null)
                {
                    openIcon = Resources.Load("favourites_open") as Texture;
                }
                return openIcon;
            }
        }

        private static Texture openFolderIcon = null;
        public static Texture OpenFolderIcon
        {
            get
            {
                if (openFolderIcon == null)
                {
                    openFolderIcon = Resources.Load("favourites_open-folder") as Texture;
                }
                return openFolderIcon;
            }
        }

        private static Texture hierarchyIcon = null;
        public static Texture HierarchyIcon
        {
            get
            {
                if (hierarchyIcon == null)
                {
                    hierarchyIcon = Resources.Load("favourites_hierarchy_toggle") as Texture;
                }
                return hierarchyIcon;
            }
        }

        private static Texture instantiateIcon = null;
        public static Texture InstantiateIcon
        {
            get
            {
                if (instantiateIcon == null)
                {
                    instantiateIcon = Resources.Load("favourites_instantiate") as Texture;
                }
                return instantiateIcon;
            }
        }

        private static Texture favouriteSetIcon = null;
        public static Texture FavouriteSetIcon
        {
            get
            {
                if (favouriteSetIcon == null)
                {
                    favouriteSetIcon = Resources.Load("favourites_large_icon") as Texture;
                }
                return favouriteSetIcon;
            }
        }

        private static Texture addSetIcon = null;
        public static Texture AddSetIcon
        {
            get
            {
                if (addSetIcon == null)
                {
                    addSetIcon = Resources.Load("favourites_add") as Texture;
                }
                return addSetIcon;
            }
        }

        private static GUIContent removeButtonContent = null;
        public static GUIContent RemoveButtonContent
        {
            get
            {
                if (removeButtonContent == null)
                {
                    removeButtonContent = new GUIContent();
                    removeButtonContent.tooltip = !Preferences.ShowTooltips ?  string.Empty : RemoveButtonTooltip;
                }
                return removeButtonContent;
            }
        }

        private static GUIContent openAssetContent = null;
        public static GUIContent OpenAssetContent
        {
            get
            {
                if (openAssetContent == null)
                {
                    openAssetContent = new GUIContent(OpenIcon, !Preferences.ShowTooltips ?  string.Empty : OpenAssetTooltip);
                }
                return openAssetContent;
            }
        }

        private static GUIContent instantiatePrefabContent = null;
        public static GUIContent InstantiatePrefabContent
        {
            get
            {
                if (instantiatePrefabContent == null)
                {
                    instantiatePrefabContent = new GUIContent(InstantiateIcon, !Preferences.ShowTooltips ?  string.Empty : InstantiatePrefabTooltip);
                }
                return instantiatePrefabContent;
            }
        }

        private static GUIContent openFolderContent = null;
        public static GUIContent OpenFolderContent
        {
            get
            {
                if (openFolderContent == null)
                {
                    openFolderContent = new GUIContent(OpenFolderIcon, !Preferences.ShowTooltips ?  string.Empty : OpenFolderTooltip);
                }
                return openFolderContent;
            }
        }

        private static GUIContent hierarchyToggleContent = null;
        public static GUIContent HierarchyToggleContent
        {
            get
            {
                if (hierarchyToggleContent == null)
                {
                    hierarchyToggleContent = new GUIContent(HierarchyIcon, !Preferences.ShowTooltips ?  string.Empty : HierarchyToggleTooltip);
                }
                return hierarchyToggleContent;
            }
        }

        private static GUIContent favouriteSetContent = null;
        public static GUIContent FavouriteSetContent
        {
            get
            {
                if (favouriteSetContent == null)
                {
                    favouriteSetContent = new GUIContent(FavouriteSetIcon, !Preferences.ShowTooltips ?  string.Empty : FavouriteSetTooltip);
                }
                return favouriteSetContent;
            }
        }

        private static GUIContent primaryButtonContent = null;
        public static GUIContent PrimaryButtonContent
        {
            get
            {
                if (primaryButtonContent == null)
                {
                    primaryButtonContent = new GUIContent();
                    primaryButtonContent.tooltip =  !Preferences.ShowTooltips ?  string.Empty : PrimaryButtonTooltip;
                }
                return primaryButtonContent;
            }
        }

        private static GUIContent addSetContent = null;
        public static GUIContent AddSetContent
        {
            get
            {
                if (addSetContent == null)
                {
                    addSetContent = new GUIContent(AddSetIcon, !Preferences.ShowTooltips ?  string.Empty : AddSetTooltip);
                }
                return addSetContent;
            }
        }

        public static void UpdateTooltips ()
        {
            PrimaryButtonContent.tooltip = !Preferences.ShowTooltips ?  string.Empty : PrimaryButtonTooltip;
            HierarchyToggleContent.tooltip = !Preferences.ShowTooltips ?  string.Empty : HierarchyToggleTooltip;
            OpenFolderContent.tooltip = !Preferences.ShowTooltips ?  string.Empty : OpenFolderTooltip;
            InstantiatePrefabContent.tooltip = !Preferences.ShowTooltips ?  string.Empty : InstantiatePrefabTooltip;
            OpenAssetContent.tooltip = !Preferences.ShowTooltips ?  string.Empty :  OpenAssetTooltip;
            RemoveButtonContent.tooltip = !Preferences.ShowTooltips ?  string.Empty :  RemoveButtonTooltip;
        }

        private static string PrimaryButtonTooltip = "View/Edit this asset in the Project Window & Inspector. Double-click to open the asset in the default application";
        private static string HierarchyToggleTooltip = "Show/Hide this prefab's children";
        private static string OpenFolderTooltip = "Open this folder in the OS";
        private static string InstantiatePrefabTooltip = "Create an instance of this prefab in the current scene";
        private static string OpenAssetTooltip = "Open this asset in the default application";
        private static string RemoveButtonTooltip = "Remove this favourite from the current set";
        private static string FavouriteSetTooltip = "Open this favourites set";
        private static string AddSetTooltip = "Create a new favourites set";
        //private static string FavouriteSetToolbarTooltip = "Select the active favourites set";

    }
}
