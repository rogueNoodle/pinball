﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Roguenoodle.Favourites
{
    public class FavouritesWindow : EditorWindow 
    {

        private static FavouritesWindow instance;
        public static FavouritesWindow Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = GetWindow<FavouritesWindow>();
                }
                return instance;
            }
        }

        [MenuItem ("Window/Favourites")]
        private static void ShowWindow ()
        {
            if (Instance != null)
            {
                Instance.titleContent = new GUIContent("Favourites", Styles.StarIcon);
                //Instance.wantsMouseMove = true;
            }
        }

        public static void SaveCurrentSet()
        {
            if (Instance != null)
            {
                if (Instance.CurrentSet != null)
                {
                    EditorUtility.SetDirty(Instance.CurrentSet);
                    AssetDatabase.SaveAssets();
                }
            }
        }

        public static void RequestRemoval(bool undoSupport = true)
        {
            //RemovalRequested = true;
            if (Instance == null)
                return;

            if (Instance.CurrentSet != null)
            {
                if (undoSupport)
                    Undo.RegisterCompleteObjectUndo(Instance.CurrentSet, "Remove Favourite");

                Instance.CurrentSet.RemoveQueuedFavourites();
                EditorUtility.SetDirty(Instance.CurrentSet);
                AssetDatabase.SaveAssets();
                Instance.Repaint();
            }
        }

        public static void RepaintWindow ()
        {
            if (Instance != null)
                Instance.Repaint();
        }


        private void OnEnable ()
        {
            UpdateFavouriteSets();
            EditorApplication.projectWindowChanged += OnProjectWindowChanged;
            Undo.undoRedoPerformed += OnUndoPerformed;
        }

        #region INSTANCE FIELDS & METHODS


        private FavouriteSet currentSet;
        public FavouriteSet CurrentSet
        {
            get
            {
                if (CurrentSetIndex < 0)
                    return null;
                
                string path = AssetDatabase.GUIDToAssetPath(FavouriteSetGUIDs[CurrentSetIndex]);
                if (path != string.Empty)
                {
                    currentSet = AssetDatabase.LoadAssetAtPath<FavouriteSet>(path) as FavouriteSet;
                }

                return currentSet;
            }
        }

        private List<string> favouriteSetGUIDs;
        public List<string> FavouriteSetGUIDs
        {
            get
            {
                if (favouriteSetGUIDs == null)
                {
                    favouriteSetGUIDs = new List<string> ();
                }
                return favouriteSetGUIDs;
            }
        }

        private int currentSetIndex = 0;
        public int CurrentSetIndex
        {
            get
            {
                currentSetIndex = Mathf.Clamp(currentSetIndex, -1, FavouriteSetGUIDs.Count-1);
                return currentSetIndex;
            }
            set
            {
                currentSetIndex = Mathf.Clamp(value, -1, FavouriteSetGUIDs.Count-1);
            }
              
        }

        private Vector2 scrollPosition;

        public string[] GetFavouriteSetNames ()
        {
            string[] names = new string[FavouriteSetGUIDs.Count];
            for (int i = 0; i < FavouriteSetGUIDs.Count; i++)
            {
                string name = AssetDatabase.GUIDToAssetPath(FavouriteSetGUIDs[i]);
                name = Path.GetFileNameWithoutExtension(name);
                names[i] = name;
            }
            return names;
        }

        private void OnGUI ()
        {
            DrawToolbar ();
            dropbox = EditorGUILayout.BeginVertical();

            dropbox.width --;
            dropbox.x++;
            dropbox.height--;
            dropbox.y--;
            DrawFavourites ();
            DropAreaGUI (dropbox);
            if (mouseInsideDropBox)
            {
                Color cachedColor = GUI.backgroundColor;
                GUI.backgroundColor = Styles.AccentColor;
                GUI.Box (dropbox, GUIContent.none, Styles.DropBox);
                GUI.backgroundColor = cachedColor;
            }

            EditorGUILayout.EndVertical();

        }

        private static IEnumerable<Object> droppedObjects;

        private void DropAreaGUI (Rect dropZone)
        {
            Event evt = Event.current;

//            dropZone.width -= 20;
//            dropZone.x += 10;
//            dropZone.height -= 20;
//            dropZone.y += 10;
           
            switch (evt.type) {
                case EventType.DragExited:
                    mouseInsideDropBox = false;
                    break;
                case EventType.DragUpdated:
                case EventType.DragPerform:
                    if (!dropZone.Contains (evt.mousePosition))
                        mouseInsideDropBox = false;

                    mouseInsideDropBox = true;
                    DragAndDrop.visualMode = DragAndDropVisualMode.Link;

                    if (evt.type == EventType.DragPerform) {
                        DragAndDrop.AcceptDrag ();

                        if (CurrentSet == null)
                        {
                            Instance.CreateFavouriteSet(false);
                        }

                        droppedObjects = DragAndDrop.objectReferences;
                        EditorApplication.delayCall = AddDroppedAssets;


                    }
                    break;
            }
        }

        private void AddDroppedAssets ()
        {
            if (droppedObjects == null)
                return;

            if (droppedObjects.Count() <= 0)
                return;
            
            foreach (Object obj in droppedObjects) {
                string path = AssetDatabase.GetAssetPath(obj);
                if (path != null && path != string.Empty)
                {
                    string GUID = AssetDatabase.AssetPathToGUID(path);
                    if (GUID != null && GUID != string.Empty)
                    {
                        AssetFavourite fav = new AssetFavourite(GUID);
                        if (fav != null)
                        {
                            CurrentSet.AddFavourite(fav);
                        }
                    }
                }
            }
        }



        private bool mouseInsideDropBox = false;
        private Rect dropbox = new Rect(0,0,0,0);



        public void OnProjectWindowChanged ()
        {
            UpdateFavouriteSets();
        }

        public void OnUndoPerformed ()
        {
            Repaint();

            if (CurrentSet != null)
            {
                CurrentSet.RemoveQueuedFavourites();
                EditorUtility.SetDirty(CurrentSet);
                AssetDatabase.SaveAssets();
            }
        }

        private Rect DrawToolbar ()
        {
            Rect toolbarRect = EditorGUILayout.BeginHorizontal (Styles.Toolbar);
            {
                GUILayout.Label("Current Set", Styles.ToolbarLabel);

                // Set loading popup
                int newIndex = EditorGUILayout.Popup(CurrentSetIndex, GetFavouriteSetNames(), Styles.ToolbarPopup);
                if (newIndex != CurrentSetIndex)
                {
                    CurrentSetIndex = newIndex;
                }
                Color cachedColor = GUI.contentColor;
                GUI.contentColor = Styles.AccentColor;
                if (GUILayout.Button(Styles.AddSetContent, Styles.ToolbarButton ))
                {
                    CreateFavouriteSet ();
                }
                GUI.contentColor = cachedColor;
            }
            EditorGUILayout.EndHorizontal ();
            return toolbarRect;
        }

        private void DrawFavourites ()
        {
            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, Styles.ScrollView);
            {
                if (CurrentSet != null)
                    CurrentSet.Draw();
            }
            EditorGUILayout.EndScrollView();
        }

        private void CreateFavouriteSet (bool selectSet = true)
        {
            string setPath = FavouriteSet.Create();
            UpdateFavouriteSets();
            CurrentSetIndex = FavouriteSetGUIDs.IndexOf(AssetDatabase.AssetPathToGUID(setPath));
            if (selectSet)
            {
                Selection.activeObject = AssetDatabase.LoadAssetAtPath<Object>(setPath) as Object;
                EditorUtility.FocusProjectWindow();
            }
            Repaint();
        }

        public void SelectFavouriteSet (FavouriteSet favSet)
        {
            string path = AssetDatabase.GetAssetPath(favSet);
            if (path == null)
                return;

            string GUID = AssetDatabase.AssetPathToGUID(path);
            if (GUID == null)
                return;
            
            for (int i = 0; i < FavouriteSetGUIDs.Count; i++)
            {
                if (GUID == FavouriteSetGUIDs[i])
                    CurrentSetIndex = i;
            }
        }

        /// <summary>
        /// Updates the list of favourite sets
        /// </summary>
        private void UpdateFavouriteSets ()
        {
            FavouriteSetGUIDs.Clear();
            string[] guids = AssetDatabase.FindAssets("t:FavouriteSet");
            FavouriteSetGUIDs.AddRange(guids.ToList());
            Repaint();
        }

        [MenuItem("Assets/Add Favourite(s)", false, 0)]
        private static void AddAssetFavourites()
        {
            if (Instance == null)
                return;

            if (Instance.CurrentSet == null)
                return;

            Undo.RegisterCompleteObjectUndo(Instance.CurrentSet, "Add Favourites");
            foreach (string GUID in Selection.assetGUIDs)
            {
                Instance.CurrentSet.AddFavourite(new AssetFavourite(GUID));
            }

            EditorUtility.SetDirty(Instance.CurrentSet);
            AssetDatabase.SaveAssets();
            Instance.Repaint();
        }

        [MenuItem("Assets/Add Favourite(s)", true)]
        private static bool ValidateAddAssetFavourites()
        {
            if (Selection.assetGUIDs.Length > 0)
                return true;

            return false;
        }

        [MenuItem("Assets/Create Favourite Set From Selection", false, 0)]
        private static void CreateNewSetFromSelection()
        {
            if (Instance == null)
                return;

            Instance.CreateFavouriteSet(false);
            AddAssetFavourites();
            Selection.activeObject = Instance.CurrentSet as Object;
            EditorUtility.FocusProjectWindow();
        }
            

        [MenuItem("Assets/Create Favourite Set From Selection", true)]
        private static bool ValidateCreateNewSetFromSelection()
        {
            if (Selection.assetGUIDs.Length > 0)
                return true;

            return false;
        }

            

        #endregion

        private static GameObject latestInstantiatedGO;

        public static void InstantiatePrefab (GameObject gameObject)
        {

            if (gameObject == null)
                return;

           
            // If there's an actively selected transform, this becomes the parent to the spawned object
            Transform parent = Selection.activeTransform;

            // Create a temporary object as a way of accessing Unity's default spawn position
            EditorApplication.ExecuteMenuItem("GameObject/Create Empty");
            GameObject temp = Selection.activeGameObject;
            Vector3 position = temp.transform.position;
            GameObject.DestroyImmediate (temp);



            // Create an instance of the prefab and focus in sceneview
            latestInstantiatedGO = PrefabUtility.InstantiatePrefab(gameObject) as GameObject;
            latestInstantiatedGO.transform.SetParent(parent);
            latestInstantiatedGO.transform.localPosition = position;
            Undo.RegisterCreatedObjectUndo(latestInstantiatedGO, "Create Instance of " + gameObject.name);

            EditorApplication.delayCall += SelectLatestInstantiatedGO;
            //SceneView.lastActiveSceneView.FrameSelected();
        }

        private static void SelectLatestInstantiatedGO ()
        {
            Selection.activeGameObject = latestInstantiatedGO;
        }
    }
}


