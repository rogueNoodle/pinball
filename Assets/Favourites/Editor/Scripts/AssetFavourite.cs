﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Roguenoodle.Favourites
{
    [System.Serializable]
    public class AssetFavourite : IFavourite
    {


        public AssetFavourite (string GUID)
        {
            _GUID = GUID;
        }
            
        private Object asset;
        public Object Asset
        {
            get 
            {
                if (asset == null)
                {
                    string path = AssetDatabase.GUIDToAssetPath(GUID);
                    if (path != string.Empty)
                    {
                        asset = AssetDatabase.LoadAssetAtPath<Object>(path) as Object;
                    }
                    if (asset == null)
                    {
                        shouldRemove = true;
                        FavouritesWindow.RequestRemoval(false);
                    }
                }
                return asset;
            }
        }

        [SerializeField] private string _GUID;
        public string GUID
        {
            get { return _GUID; }
        }


        public bool IsGameObject
        {
            get { return Asset as GameObject != null; }
        }

        public bool IsFolder
        {
            get { return AssetDatabase.IsValidFolder(AssetDatabase.GUIDToAssetPath(GUID)); }
        }

        public bool IsFavouriteSet
        {
            get { 
                FavouriteSet favSet = Asset as FavouriteSet;
                return favSet != null;
            }
        }

        public bool IsBlendFile
        {
            get { return (Extension == ".blend"); }
        }

        #region IFavourite implementation
        public void MainAction()
        {
            Selection.activeObject = Asset;
        }

        public void SecondaryAction()
        {
            if (Asset == null)
                ShouldRemove = true;

            if (IsBlendFile)
            {
                AssetDatabase.OpenAsset(Asset);
            }
            else if (IsGameObject)
            {
                FavouritesWindow.InstantiatePrefab(Asset as GameObject);
            }
            else if (IsFolder)
            {
                EditorUtility.RevealInFinder(AssetDatabase.GUIDToAssetPath(GUID));
            }
            else if (IsFavouriteSet)
            {
                FavouritesWindow.Instance.SelectFavouriteSet(Asset as FavouriteSet);
            }
            else
            {
                AssetDatabase.OpenAsset(Asset);
            }
        }

        public void DrawExtras()
        {
            GameObject go = Asset as GameObject;
            if (go != null)
            {
                Transform[] children = go.GetComponentsInChildren<Transform>(true);
                float spacer = Styles.FavouriteHeight * 1.75f;

                Color cachedGUIColor;
                Color spacerColor = Styles.AccentColor;
                spacerColor.a = 0.2f;

                foreach (Transform child in children)
                {
                    if (child.gameObject != go)
                    {
                        Rect box = EditorGUILayout.BeginHorizontal(Styles.HierarchyItemContainer);
                        box.width = spacer;
//
//                        cachedGUIColor = GUI.color;
//                        GUI.color = spacerColor;
//                        GUI.DrawTexture(box, Styles.WhiteBlock);
//                        GUI.color = cachedGUIColor;
                        GUILayout.Space (spacer);

                        cachedGUIColor = GUI.backgroundColor;
                        GUI.backgroundColor = Styles.AccentColor;
                        for (int i = 0; i < (child.gameObject.GetComponentsInParent<Transform>(true).Length - 1); i++)
                        {
                            GUILayout.Box(GUIContent.none, Styles.HierarchyIndicator);
                        }
                        GUI.backgroundColor =cachedGUIColor;

                        //cachedGUIColor = GUI.backgroundColor;
                        //GUI.backgroundColor = Styles.HierarchyItemTint;
                        Styles.HierarchyItem.fixedWidth = EditorGUIUtility.currentViewWidth - (Styles.FavouriteHeight * 2.75f);

                        GUIContent content = EditorGUIUtility.ObjectContent(child.gameObject as Object, typeof(Object));
                        content.tooltip = "Click to view/edit this child in the inspector";
                            
                        if (GUILayout.Button(content, Styles.HierarchyItem))
                        {
                            Selection.activeObject = child.gameObject;
                        }
                        //GUI.backgroundColor = cachedGUIColor;

                        EditorGUILayout.EndHorizontal();
                    }
                }
            }

        }

        [SerializeField] private bool isPinned = false;
        public bool IsPinned
        {
            get
            {
                return isPinned;
            }
            set
            {
                isPinned = value;
            }
        }

        private bool shouldRemove = false;
        public bool ShouldRemove
        {
            get
            {
                return shouldRemove;
            }
            set
            {
                shouldRemove = value;
            }
        }

        public float Height
        {
            get
            {
                return 42;
            }
        }

        public Texture MainIcon
        {
            get
            {
                return AssetDatabase.GetCachedIcon(AssetDatabase.GUIDToAssetPath(GUID));
            }
        }

        private string mainLabel = string.Empty;
        public string MainLabel
        {
            get
            {
                if (mainLabel == string.Empty || mainLabel == null)
                {
                    string path = AssetDatabase.GUIDToAssetPath(GUID);
                    if (path != string.Empty || path != null)
                    {
                        if (AssetDatabase.IsValidFolder(path))
                        {
                            string[] pathComponents = path.Split(Path.DirectorySeparatorChar);
                            mainLabel = pathComponents.Last<string>();
                        }
                        else
                        {
                            mainLabel = Path.GetFileNameWithoutExtension(path);
                            extension = Path.GetExtension(path);
                        }
                    }
                    if (mainLabel == string.Empty || mainLabel == null)
                    {
                        shouldRemove = true;
                    }
                }
                return mainLabel;
            }
        }

        private string extension = string.Empty;
        public string Extension
        {
            get
            {
                if (extension == string.Empty || extension == null)
                {
                    string path = AssetDatabase.GUIDToAssetPath(GUID);
                    if (path != string.Empty || path != null)
                    {
                        if (AssetDatabase.IsValidFolder(path))
                        {
                            string[] pathComponents = path.Split(Path.DirectorySeparatorChar);
                            mainLabel = pathComponents.Last<string>();
                        }
                        else
                        {
                            mainLabel = Path.GetFileNameWithoutExtension(path);
                            extension = Path.GetExtension(path);
                        }
                    }
                }
                return extension;
            }
        }

        private string secondaryLabel;
        public string SecondaryLabel
        {
            get
            {
                if (secondaryLabel == string.Empty || secondaryLabel == null)
                {
                    if (Asset != null)
                    {
                        if (IsFolder)
                        {
                            secondaryLabel = "Folder";
                        }
                        else
                        {
                            string type = Asset.GetType().ToString();
                            string[] splitType = type.Split('.');
                            secondaryLabel = splitType.Last<string>();
                        }
                    }
                    else
                    {
                        secondaryLabel = "Null Type";
                    }
                }
                return secondaryLabel;
            }
        }



        public GUIContent SecondaryActionContent
        {
            get
            {
                if (IsBlendFile)
                {
                    return Styles.OpenAssetContent;
                }
                else if (IsGameObject)
                {
                    return Styles.InstantiatePrefabContent;
                }
                else if (IsFolder)
                {
                    return Styles.OpenFolderContent;
                }
                else if (IsFavouriteSet)
                {
                    return Styles.FavouriteSetContent;
                }
                return Styles.OpenAssetContent;
            }
        }

        public Texture SecondaryActionIcon
        {
            get
            {
                return Styles.OpenIcon;
            }
        }

        private bool hasExtrasChecked = false;
        private bool hasExtras;
        public bool HasExtras
        {
            get
            {
                if (hasExtrasChecked == false)
                {
                    hasExtras = false;
                    if (Asset != null)
                    {
                        GameObject go = Asset as GameObject;
                        if (go != null)
                        {
                            if (go.GetComponentsInChildren<Transform>().Length > 1)
                                hasExtras = true;
                        }
                    }
                    hasExtrasChecked = true;
                }
                return hasExtras;
            }
        }

        public GUIContent ToggleExtrasContent
        {
            get
            {
                return Styles.HierarchyToggleContent;
            }
        }

        [SerializeField] private bool shouldDrawExtras = false;
        public bool ShouldDrawExtras
        {
            get
            {
                return shouldDrawExtras;
            }
            set
            {
                shouldDrawExtras = value;
            }
        }
        #endregion
    }
}

