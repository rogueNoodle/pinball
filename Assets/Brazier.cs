﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brazier : MonoBehaviour {

    [SerializeField] GameObject particleSystemGO;
    [SerializeField] GameObject lightGO;
	
    public void Toggle ()
    {
        particleSystemGO.SetActive(!particleSystemGO.activeSelf);
        lightGO.SetActive(!lightGO.activeSelf);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            Toggle();
        }
    }
}
