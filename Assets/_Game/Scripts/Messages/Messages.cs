﻿using UnityEngine;

public static class SceneMessages
{
    public static string SceneLoadComplete = "SMSceneLoadComplete";
    public static string SceneFadeInComplete = "SNSceneFadeInComplete";
}

public static class GameMessages
{
    public static string TableComplete = "GMTableComplete";
    public static string TableLoaded = "GMTableLoaded";
}

public static class ObjectiveMessages
{
    public static string RegisterObjective = "OM_RegisterObjective";
    public static string ObjectivesUpdated = "OM_ObjectivesUpdated";
    public static string ObjectiveCompleted = "OM_ObjectiveCompleted";
    public static string ObjectiveAdded = "OM_ObjectiveAdded";
}

public static class PlayerMessages
{
    public static string RingOut = "PMRingOut";
    public static string ConsciousStateChanged = "PlayerMessages_ConsciousStateChanged";
}

public static class ActorMessages
{
    public static string StatusChange = "ActorMessages_StatusChangeMessage";
}

public struct StatusChangeData
{
    public Color color;
    public string text;
    public Vector3 position;
    public StatusChangeData (Vector3 aPosition, Color aColor, string someText)
    {
        position = aPosition;
        color = aColor;
        text = someText;
    }
       
}
