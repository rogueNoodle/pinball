﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CharacterClass : ScriptableObject 
{
    [Tooltip("Stats at level 0")]
    [SerializeField] private Stat strength = new Stat("Strength", "STR");
    [SerializeField] private Stat agility = new Stat("Agility", "AGI");
    [SerializeField] private Stat vitality = new Stat("Vitality", "VIT");
    [SerializeField] private Stat intelligence = new Stat("Intelligence", "INT");
    [SerializeField] private Ability classAbility;
}

[System.Serializable]
public class Stat
{
    [SerializeField] private string name = "Stat";
    [SerializeField] private string abbreviation = "STA";
    [SerializeField] private int startValue = 0;
    [SerializeField] private int maxValue = 100;

    public Stat ()
    {

    }

    public Stat (string name, string abbreviation)
    {
        this.name = name;
        this.abbreviation = abbreviation;
    }

}
