﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[CreateAssetMenu]
public class LightingParameters : ScriptableObject {

    #if UNITY_EDITOR 
    public UnityEditor.LightmapParameters lightmapParameters;
    #endif
    public Material skybox;
    public AmbientMode ambientMode;
    public Color skyColor;
    public Color equatorColor;
    public Color groundColor;
    public float reflectionIntensity = 1;
    public int reflectionBounces = 1;

}
