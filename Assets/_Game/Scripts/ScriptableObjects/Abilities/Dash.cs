﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

[CreateAssetMenu (menuName="Abilities/Dash")]
public class Dash : Ability 
{
    protected override void Trigger()
    {
        base.Trigger();

        Vector3 aim = PlayerInput.Aim;
        if (aim.sqrMagnitude > 0)
        {
            Rigidbody.velocity = aim * Ball.MaxSpeed;
        }
        else
        {
            Rigidbody.velocity = (Rigidbody.velocity.sqrMagnitude > 0 ? Rigidbody.velocity.normalized : Rigidbody.transform.forward) * Ball.MaxSpeed;
        }
    }

}
