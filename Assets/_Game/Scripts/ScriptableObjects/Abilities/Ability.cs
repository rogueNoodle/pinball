﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Ability : ScriptableObject 
{

    [SerializeField] private string displayName = "Base Ability";
    [SerializeField] private string description = "Base ability - this does nothing. Don't USE";
    [SerializeField] private string useSoundName = "";
    [SerializeField] private string readySoundName = "";
    [SerializeField] private Sprite icon;
    [SerializeField] private float staminaCost = 0;
    [SerializeField] private float cooldown = 0;
    [SerializeField] private bool requiresAim = false;

    private GameObject gameObject;
    public GameObject GameObject
    {
        get
        {
            return gameObject;
        }
    }

    private Ball ball;
    public Ball Ball
    {
        get {
            if (ball == null)
            {
                ball = gameObject.GetComponent<Ball>();
            }
            return ball;
        }
    }

    private Rigidbody rigidbody;
    public Rigidbody Rigidbody
    {
        get {
            if (rigidbody == null)
            {
                rigidbody = gameObject.GetComponent<Rigidbody>();
            }
            return rigidbody;
        }
    }

    protected virtual void Initialize(GameObject obj)
    {
        gameObject = obj;
    }

    protected virtual void Trigger()
    {
        if (requiresAim)
        {
            if (PlayerInput.Aim.sqrMagnitude == 0)
            {
                return;
            }
        }

        Debug.Log("Ability Triggered : " + this.GetType().ToString());
    }

}

public static class AbilityMessage
{
    public static string RegisterAbility = "AMRegisterAbility";
}
