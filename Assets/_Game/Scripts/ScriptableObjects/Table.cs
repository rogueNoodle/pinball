﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu (menuName="Pinball/Table")]
public class Table : ScriptableObject
{
    public string baseSceneName;
    public List<string> configurationSceneNames;
    public string ambientSoundName;

    public string RandomConfiguration ()
    {
        return configurationSceneNames[Random.Range(0, configurationSceneNames.Count)];
    }
}

