﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour {

    [SerializeField]
    private float minIntensity = 0.2f;

    [SerializeField]
    private float maxIntensity = 1f;

    [SerializeField]
    private float smoothTime = 0.1f;

    private float velocity = 0;

    new private Light light;

    private void Awake()
    {
        light = GetComponent<Light>();
    }
	
	// Update is called once per frame
	void Update () {

        light.intensity = Mathf.SmoothDamp(light.intensity, Random.Range(minIntensity, maxIntensity), ref velocity, smoothTime);
	}
}
