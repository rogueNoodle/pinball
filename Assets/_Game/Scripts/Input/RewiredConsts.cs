/* Rewired Constants
   This list was generated on 2/24/2017 10:07:57 AM
   The list applies to only the Rewired Input Manager from which it was generated.
   If you use a different Rewired Input Manager, you will have to generate a new list.
   If you make changes to the exported items in the Rewired Input Manager, you will need to regenerate this list.
*/

namespace RewiredConsts {
    public static class RewiredConsts {
        public static class Action {
            // Default
            // Gameplay
            [Rewired.Dev.ActionIdFieldInfo(categoryName = "Gameplay", friendlyName = "Left Flipper")]
            public const int LeftFlipper = 0;
            [Rewired.Dev.ActionIdFieldInfo(categoryName = "Gameplay", friendlyName = "Right Flipper")]
            public const int RightFlipper = 1;
            [Rewired.Dev.ActionIdFieldInfo(categoryName = "Gameplay", friendlyName = "Action0")]
            public const int AimHorizontal = 4;
            [Rewired.Dev.ActionIdFieldInfo(categoryName = "Gameplay", friendlyName = "Action0")]
            public const int AimVertical = 5;
            [Rewired.Dev.ActionIdFieldInfo(categoryName = "Gameplay", friendlyName = "Skill Slot Bottom")]
            public const int SkillSlotBottom = 10;
            [Rewired.Dev.ActionIdFieldInfo(categoryName = "Gameplay", friendlyName = "Skill Slot Left")]
            public const int SkillSlotLeft = 13;
            [Rewired.Dev.ActionIdFieldInfo(categoryName = "Gameplay", friendlyName = "Skill Slot Right")]
            public const int SkillSlotRight = 12;
            [Rewired.Dev.ActionIdFieldInfo(categoryName = "Gameplay", friendlyName = "Skill Slot Top")]
            public const int SkillSlotTop = 11;
            [Rewired.Dev.ActionIdFieldInfo(categoryName = "Gameplay", friendlyName = "Toggle Camera")]
            public const int ToggleCamera = 14;
            // UI
            [Rewired.Dev.ActionIdFieldInfo(categoryName = "UI", friendlyName = "UI Vertical")]
            public const int UIVertical = 6;
            [Rewired.Dev.ActionIdFieldInfo(categoryName = "UI", friendlyName = "UI Horizontal")]
            public const int UIHorizontal = 7;
            [Rewired.Dev.ActionIdFieldInfo(categoryName = "UI", friendlyName = "UI Cancel")]
            public const int UICancel = 8;
            [Rewired.Dev.ActionIdFieldInfo(categoryName = "UI", friendlyName = "UI Submit")]
            public const int UISubmit = 9;
        }
        public static class Category {
            public const int Default = 0;
            public const int UI = 1;
        }
        public static class Layout {
            public static class Joystick {
                public const int Default = 0;
            }
            public static class Keyboard {
                public const int Default = 0;
            }
            public static class Mouse {
                public const int Default = 0;
            }
            public static class CustomController {
                public const int Default = 0;
            }
        }
    }
}
