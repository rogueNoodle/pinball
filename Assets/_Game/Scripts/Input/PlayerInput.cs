﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using RewiredConsts;

public class PlayerInput : MonoBehaviour {

    private static PlayerInput instance = null;

    public static PlayerInput Instance
    {
        get
        {
            if (instance == null)
            {
                
                instance = FindObjectOfType<PlayerInput>();
                if (instance == null)
                {
                    GameObject playerInputGO = new GameObject("PlayerInput");
                    playerInputGO.AddComponent<PlayerInput>();
                    instance = playerInputGO.GetComponent<PlayerInput>();
                }
            }
            return instance;
        }
    }

    private Rewired.Player player;
    public Rewired.Player Player
    {
        get
        {
            if (player == null && ReInput.isReady)
            {
                player = ReInput.players.GetPlayer(0);
                Joystick joystick = player.controllers.Joysticks[0];
                if (joystick != null)
                {
                    for (int i = 0; i < joystick.calibrationMap.axisCount; i++)
                    {
                        joystick.calibrationMap.Axes[i].deadZone = 0.625f;
                    }
                }
            }
            return player;
        }
    }

    public static bool GetFlipper(string flipperSide)
    {
        if (Instance.Player == null || RN.Pinball.Player.Instance.CurrentUnconsciousnessPercentage > 0)
            return false;
        else
            return Instance.Player.GetButton(flipperSide);
    }

    public static bool GetFlipperDown(string flipperSide)
    {
        if (Instance.Player == null || RN.Pinball.Player.Instance.CurrentUnconsciousnessPercentage > 0)
            return false;
        else
            return Instance.Player.GetButtonDown(flipperSide);
    }

    private Vector3 aim = Vector3.zero;
    public static Vector3 Aim
    {
        get
        {
            if (Instance.Player == null || RN.Pinball.Player.Instance.CurrentUnconsciousnessPercentage > 0)
            {
                return Vector3.zero;
            }
            else
            {
                Instance.aim.x = Instance.Player.GetAxisRaw("AimHorizontal");
                Instance.aim.z = Instance.Player.GetAxisRaw("AimVertical");
                Instance.aim.Normalize();
                return Instance.aim;
            }

        }
    }

    public static bool GetCameraTogglePressed ()
    {
        if (Instance.player != null)
        {
            //Instance.player.controllers.Joysticks[0].SetVibration(0, 1);
            //Instance.player.controllers.Joysticks[0].SetVibration(1, 1);
        }
        return Instance.player == null ? false : Instance.player.GetButtonDown(RewiredConsts.RewiredConsts.Action.ToggleCamera);
    }

    public static bool GetSkillSlotPressed (SkillSlot skillSlot)
    {
        bool pressed = false;
        if (Instance.Player == null || RN.Pinball.Player.Instance.CurrentUnconsciousnessPercentage > 0)
            return pressed;

        switch (skillSlot)
        {
            case SkillSlot.Bottom:
                pressed = Instance.Player.GetButton(RewiredConsts.RewiredConsts.Action.SkillSlotBottom);
                break;
            case SkillSlot.Left:
                pressed = Instance.Player.GetButton(RewiredConsts.RewiredConsts.Action.SkillSlotLeft);
                break;
            case SkillSlot.Right:
                pressed = Instance.Player.GetButton(RewiredConsts.RewiredConsts.Action.SkillSlotRight);
                break;
            case SkillSlot.Top:
                pressed = Instance.Player.GetButton(RewiredConsts.RewiredConsts.Action.SkillSlotTop);
                break;
            default:
                pressed = false;
                break;
        }

        return pressed;
    }

    public static bool GetSkillSlotDown (SkillSlot skillSlot)
    {
        bool pressed = false;
        if (Instance.Player == null || RN.Pinball.Player.Instance.CurrentUnconsciousnessPercentage > 0)
            return pressed;

        switch (skillSlot)
        {
            case SkillSlot.Bottom:
                pressed = Instance.Player.GetButtonDown(RewiredConsts.RewiredConsts.Action.SkillSlotBottom);
                break;
            case SkillSlot.Left:
                pressed = Instance.Player.GetButtonDown(RewiredConsts.RewiredConsts.Action.SkillSlotLeft);
                break;
            case SkillSlot.Right:
                pressed = Instance.Player.GetButtonDown(RewiredConsts.RewiredConsts.Action.SkillSlotRight);
                break;
            case SkillSlot.Top:
                pressed = Instance.Player.GetButtonDown(RewiredConsts.RewiredConsts.Action.SkillSlotTop);
                break;
            default:
                pressed = false;
                break;
        }

        return pressed;
    }

}

