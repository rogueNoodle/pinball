﻿using UnityEngine;
using System.Collections;

public class PlayerEntity : MonoBehaviour
{
    [SerializeField] MeshRenderer armourRenderer;

    public MeshRenderer ArmourRenderer
    {
        get
        {
            return armourRenderer;
        }
    }

    [SerializeField] private Reset reset;

    public Reset Reset
    {
        get
        {
            return reset;
        }
    }

    [SerializeField] private Ball ball;

    public Ball Ball
    {
        get
        {
            return ball;
        }
    }
        
}

