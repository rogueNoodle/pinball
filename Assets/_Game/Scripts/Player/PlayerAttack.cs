﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class PlayerAttack : MonoBehaviour {

    [Tooltip("Useful for projectils that can attack without receiving player damage")]
    [SerializeField] private bool ignoresDamage = true;
    private Ball ball;

    private void Awake ()
    {
        ball = GetComponent<Ball>();
    }

	private void OnCollisionEnter(Collision collision)
    {

        Attackable target = collision.collider.GetComponent<Attackable>();
        if (target == null)
            return;

        float attackPower = collision.relativeVelocity.magnitude;
        AttackResults results = target.Attack(attackPower, collision);

        AttackDetails details = new AttackDetails();
        details.location = collision.transform.position;
        details.value = attackPower;
        MessageDispatcher.SendMessage(this, CombatMessage.PlayerAttacked, details, -1);

        if (results.DidKill)
        {
            ball.MaintainPhysics();
        }
        else
        {
            if (results.ReturnedDamage > 0 && !ignoresDamage)
            {
                details.location = transform.position;
                details.value = results.ReturnedDamage;
                MessageDispatcher.SendMessage(this, CombatMessage.PlayerReceivedDamage, details, -1);
            }
        }
    }
}

public static class CombatMessage
{
    public static string PlayerAttacked = "CMPlayerAttacked";
    public static string PlayerReceivedDamage = "CMPlayerReceivedDamage";
}

public struct AttackDetails
{
    public Vector3 location;
    public float value;
}
