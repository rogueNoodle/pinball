﻿using UnityEngine;
using System.Collections;

namespace RN.Pinball
{
    public class Player : MonoBehaviour
    {
        private static Player instance;
        public static Player Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<Player>();
                }
                return instance;
            }
        }


        #region Combatant implementation

        [SerializeField] private float tempBaseAttack = 5;
        /// <summary>
        /// Returns the players base attack, which is the sum
        /// of all gear and character base attack values
        /// </summary>
        /// <value>The base attack.</value>
        public float BaseAttack
        {
            get
            {
                return tempBaseAttack;
            }
        }

        [SerializeField] private float tempBaseDefense = 2;
        public float BaseDefense
        {
            get
            {
                return tempBaseDefense;
            }
        }

        [SerializeField] private Stamina stamina;

        public Stamina Stamina
        {
            get
            {
                if (stamina == null)
                {
                    stamina = GetComponent<Stamina>();
                }
                return stamina;
            }
        }

        [SerializeField] private float currentUnconsciousnessPercentage;

        public float CurrentUnconsciousnessPercentage
        {
            get
            {
                return currentUnconsciousnessPercentage;
            }
            set
            {
                currentUnconsciousnessPercentage = value;
            }
        }


        public bool ApplyDamage(float damage)
        {
            Stamina.RemoveStamina(damage);
            return false;
        }

        [SerializeField] private Stat attackStat;
        public Stat AttackStat
        {
            get
            {
                return attackStat;
            }
        }

        [SerializeField] private Stat defenseStat;
        public Stat DefenseStat
        {
            get
            {
                return defenseStat;
            }
        }

        [SerializeField] private Stat criticalChanceStat;
        public Stat CriticalChanceStat
        {
            get
            {
                return criticalChanceStat;
            }
        }

        [SerializeField] private Stat criticalValueStat;
        public Stat CriticalValueStat
        {
            get
            {
                return criticalChanceStat;
            }
        }
        #endregion
    }
}


