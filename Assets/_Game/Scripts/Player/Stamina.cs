﻿using UnityEngine;
using System.Collections;

public class Stamina : MonoBehaviour
{
    
    [SerializeField] private float max = 15f;

    public float Max
    {
        get
        {
            return max;
        }
    }

    [SerializeField] private float regainRate = 1;

    private float current;

    public float Current
    {
        get
        {
            return current;
        }
    }

    public float Percentage 
    {
        get
        {
            return current / max;
        }
    }

    public float RemoveStamina(float amount)
    {
        current = current - Mathf.Abs(amount);
        float overkill = 0;
        if (current < 0)
        {
            overkill = Mathf.Abs(current);
            current = 0;
        }
        return overkill;
    }

    public void Add(float amount)
    {
        current = Mathf.Clamp(current + Mathf.Abs(amount), 0, Max);
        Debug.LogFormat("Stamina percentage = {0}", Percentage);
    }


    private void Awake ()
    {
        current = max;
    }

//    private void Update ()
//    {
//        current = Mathf.Clamp(current + (regainRate * Time.deltaTime), 0, max);
//    }
}

