﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class PlayerStats : MonoBehaviour
{

    private static PlayerStats instance;

    public static PlayerStats Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<PlayerStats>();
            }
            return instance;
        }
    }

    private float maxStamina = 100f;

    public float MaxStamina
    {
        get
        {
            return maxStamina;
        }
    }

    private float currentStamina = 100f;

    public float CurrentStamina
    {
        get
        {
            return currentStamina;
        }
    }

    private float maxUnconcious = 10f;

    public float MaxUnconcious
    {
        get
        {
            return maxUnconcious;
        }
    }

    private float currentUnconcious = 0;

    public float CurrentUnconcious
    {
        get
        {
            return currentUnconcious;
        }
    }


}

