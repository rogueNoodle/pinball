﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.ootii.Messages;

public class BasicHealth : Health 
{
    [SerializeField] protected float maxHealth;
    [SerializeField] protected GameObject debrisPrefab;
    [SerializeField] protected UnityEvent onKillEvent;
    protected float currentHealth;

    private void Awake ()
    {
        currentHealth = maxHealth;
    }

    #region implemented abstract members of Health

    public override bool ApplyDamage(float damage, Actor attacker)
    {
        currentHealth = Mathf.Clamp(currentHealth - damage, 0, maxHealth);
        Debug.LogFormat("{0} received {1} damage from {2}", name, damage, attacker.name);
        MessageDispatcher.SendMessage(this, ActorMessages.StatusChange, new StatusChangeData(transform.position, Color.white, damage.ToString("f1")), 0);
        return currentHealth <= 0;
    }

    public override void Kill(Vector3 killPoint, float attackSpeed)
    {
        if (debrisPrefab != null)
        {
            GameObject debrisGO = GameObject.Instantiate(debrisPrefab, transform.position, transform.rotation) as GameObject;
            Debris debris = debrisGO.GetComponent<Debris>();
            if (debris != null)
            {
                debris.Force = attackSpeed;
                debris.ExplosionPosition = killPoint;
            }
        }
        onKillEvent.Invoke();
        Destroy(gameObject);
    }

    #endregion



}
