﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollisionAttack : CollisionAttack {
    #region implemented abstract members of CollisionAttack

    public override float Calculate(float speed, float angle)
    {
        return Random.Range(minDamage, maxDamage);
    }

    public override void PerformKillshot(Actor actorKilled)
    {
        
    }

    #endregion


	
}
