﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Actor : MonoBehaviour 
{

    public virtual Vector3 Forward
    {
        get
        {
            return transform.forward;
        }
    }

    private Health health;

    public Health Health
    {
        get
        {
            return health;
        }
    }

    private CollisionAttack collisionAttack;

    public CollisionAttack CollisionAttack
    {
        get
        {
            return collisionAttack;
        }
    }

    private Defense defense;

    public Defense Defense
    {
        get
        {
            return defense;
        }
    }

    protected virtual void Awake ()
    {
        health = GetComponent<Health>();
        defense = GetComponent<Defense>();
        collisionAttack = GetComponent<CollisionAttack>();
    }


    private static Actor attacker = null;
    private static Actor defender = null;

    public static void ActorVSActorCollisionAttack(Actor actorA, Actor actorB, Collision collision)
    {

        // Neither actor capable of collision based attacks
        if (actorA.CollisionAttack == null && actorB.CollisionAttack == null)
        {
            return;
        }

        // Neither actor takes damage
        if (actorB.Health == null && actorB.Health == null)
        {
            return;
        }

        float attackSpeed = collision.relativeVelocity.magnitude;
        float attackAngle = Vector3.Dot(actorA.Forward, actorB.Forward);

        AssignAttackerOrder(actorA, actorB, attackSpeed, attackAngle);

        bool defenderKilled = PerformCollisionAttackOnActor(attacker, defender, attackSpeed, attackAngle, collision.contacts[0].point);

        if (!defenderKilled && defender.CollisionAttack != null)
        {
            PerformCollisionAttackOnActor(defender, attacker, attackSpeed, attackAngle, collision.contacts[0].point); 
        }
    }

    /// <summary>
    /// Assigns the attacker order to static variables "firstAttacker" and "secondAttacker"
    /// Needs to be updated to determin order based on priority when both actors are
    /// capable of attacking
    /// </summary>
    /// <param name="actorA">Actor a.</param>
    /// <param name="actorB">Actor b.</param>
    private static void AssignAttackerOrder (Actor actorA, Actor actorB, float attackSpeed, float attackAngle )
    {
        attacker = null;
        defender = null;

        if (actorA.CollisionAttack != null && actorB.CollisionAttack == null)
        {
            attacker = actorA;
            defender = actorB;
        }
        else if (actorB.CollisionAttack != null && actorA.CollisionAttack == null)
        {
            attacker = actorB;
            defender = actorA;
        }
        else
        {
            // This part needs work
            attacker = actorA;
            defender = actorB;
        }
    }

    private static bool PerformCollisionAttackOnActor (Actor attacker, Actor defender, float attackSpeed, float attackAngle, Vector3 hitLocation)
    {
        // Defender doesn't take damage, so attack has no effect
        if (defender.Health == null)
        {
            return false;
        }

        float attackPower = attacker.CollisionAttack.Calculate(attackSpeed, attackAngle);

        if (defender.Defense != null)
        {
            float defensePower = 0;
            defender.Defense.Calculate(attackSpeed, attackAngle);
            attackPower = attackPower - defensePower;
        }

        if (attackPower > 0)
        {
            bool defenderWasKilled = defender.Health.ApplyDamage(attackPower, attacker);
            if (defenderWasKilled)
            {
                defender.Health.Kill(hitLocation, attackSpeed);
                attacker.CollisionAttack.PerformKillshot(defender);
            }
            return defenderWasKilled;
        }
        return false;
    }
}
