﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CollisionAttack : MonoBehaviour {
    [SerializeField] protected float minDamage;
    [SerializeField] protected float maxDamage;
    public abstract float Calculate(float speed, float angle);
    public abstract void PerformKillshot(Actor actorKilled);
}
