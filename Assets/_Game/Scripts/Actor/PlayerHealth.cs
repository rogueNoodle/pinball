﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RN.Pinball;
using com.ootii.Messages;

public class PlayerHealth : Health {
    #region implemented abstract members of Health
   
    [SerializeField] private float maxStamina = 10;
    [SerializeField] private float maxUnconsciousValue = 5f;
    [SerializeField] private float maxUnconsciousRecoveryDuration = 3f;
    [SerializeField] private float percentageOfStaminaGainOnRegainConsciousness = .25f;

    private float currentStamina;
    private float currentUnconsciousValue = 0;
    private bool isConscious = true;

    private void Awake ()
    {
        currentStamina = maxStamina;
    }

    public override bool ApplyDamage(float damage, Actor attacker)
    {
        if (isConscious)
        {
            MessageDispatcher.SendMessage(this, ActorMessages.StatusChange, new StatusChangeData(transform.position, Color.red, damage.ToString("f1")), 0);
            float overkill = Player.Instance.Stamina.RemoveStamina(damage);
            if (overkill > 0)
            {
                isConscious = false;
                currentUnconsciousValue = maxUnconsciousValue;
                MessageDispatcher.SendMessage(this, PlayerMessages.ConsciousStateChanged, false, 0);
            }
        }

        return false;
    }

    public override void Kill(Vector3 killPoint, float force)
    {
        
    }

    public void ApplyHealingPercentage(float percentage)
    {
        if (isConscious)
        {
            float healingAmount = Player.Instance.Stamina.Max * percentage;
            Player.Instance.Stamina.Add(healingAmount);
            MessageDispatcher.SendMessage(this, ActorMessages.StatusChange, new StatusChangeData(transform.position, Color.white, healingAmount.ToString("f1")), 0);
        }
    }

    private void Update ()
    {
        if (!isConscious)
        {
            currentUnconsciousValue -= (maxUnconsciousValue / maxUnconsciousRecoveryDuration) * Time.deltaTime;
            if (currentUnconsciousValue <= 0)
            {
                currentUnconsciousValue = 0;
                isConscious = true;
                ApplyHealingPercentage(percentageOfStaminaGainOnRegainConsciousness);

                MessageDispatcher.SendMessage(this, PlayerMessages.ConsciousStateChanged, true, 0);
            }

            Player.Instance.CurrentUnconsciousnessPercentage = currentUnconsciousValue / maxUnconsciousValue;
        }
    }
    #endregion

}
