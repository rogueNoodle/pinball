﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionAttack : CollisionAttack {
    
    #region implemented abstract members of CollisionAttack
    private Ball ball;

    private void Awake ()
    {
        ball = GetComponent<Ball>();
    }

    public override float Calculate(float speed, float angle)
    {
        return Mathf.Lerp(minDamage, maxDamage, speed / ball.MaxSpeed);
    }

    public override void PerformKillshot(Actor actorKilled)
    {
        ball.MaintainPhysics();
    }

    #endregion


	
}
