﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Defense : MonoBehaviour {
    public abstract float Calculate(float speed, float angle);
}
