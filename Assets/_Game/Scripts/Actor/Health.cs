﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Health : MonoBehaviour
{
    /// <summary>
    /// Applies the damage and return true if the actor has died.
    /// Should send message for any skills that may reflect damage
    /// </summary>
    /// <returns><c>true</c>, if damage was applyed, <c>false</c> otherwise.</returns>
    /// <param name="attacker">Attacker.</param>
    public abstract bool ApplyDamage(float damage, Actor attacker);
    public abstract void Kill (Vector3 killPoint, float force);
}
