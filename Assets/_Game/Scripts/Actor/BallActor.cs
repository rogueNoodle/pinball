﻿using UnityEngine;
using System.Collections;

public class BallActor : Actor
{
    private new Rigidbody rigidbody;
    public Rigidbody Rigidbody
    {
        get
        {
            if (rigidbody == null)
            {
                rigidbody = GetComponent<Rigidbody>();
            }
            return rigidbody;
        }
    }

    Vector3 forward = Vector3.forward;
    public override Vector3 Forward
    {
        get
        {
            Vector3 newForward = Rigidbody.velocity.normalized;
            if (newForward.sqrMagnitude > 0)
            {
                forward = newForward;
            }
            return forward;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Actor collidedActor = collision.gameObject.GetComponent<Actor>();
        if (collidedActor != null)
        {
            Actor.ActorVSActorCollisionAttack(this, collidedActor, collision);
        }
    }
}

