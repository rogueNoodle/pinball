﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using com.ootii.Messages;

public class ObjectivesUI : MonoBehaviour {

    [SerializeField] private GameObject objectiveUIElementPrefab;

    private Dictionary<Objective, ObjectiveUIElement> objectiveUIElementsDict = new Dictionary<Objective, ObjectiveUIElement>();

    private void OnEnable ()
    {
        MessageDispatcher.AddListener(ObjectiveMessages.ObjectiveAdded, OnObjectiveAdded);
        MessageDispatcher.AddListener(ObjectiveMessages.ObjectivesUpdated, OnObjectiveUpdated);
        MessageDispatcher.AddListener(GameMessages.TableLoaded, OnTableLoaded);
    }

    private void OnDisable ()
    {
        MessageDispatcher.RemoveListener(ObjectiveMessages.ObjectiveAdded, OnObjectiveAdded);
        MessageDispatcher.RemoveListener(ObjectiveMessages.ObjectivesUpdated, OnObjectiveUpdated);
        MessageDispatcher.RemoveListener(GameMessages.TableLoaded, OnTableLoaded);
    }

    private void OnObjectiveAdded (IMessage message)
    {
       
        Objective objective = (Objective)message.Data;

        if (objective != null)
        {
            GameObject uiElementGO = Instantiate(objectiveUIElementPrefab) as GameObject;
            uiElementGO.transform.SetParent(transform, false);

            ObjectiveUIElement uiElement = uiElementGO.GetComponent<ObjectiveUIElement>();
            uiElement.IsPrimary = objective.IsPrimary;
            uiElement.SetDescription(objective.name);
            objectiveUIElementsDict.Add(objective, uiElement);
        }
    }

    private void OnObjectiveUpdated (IMessage message)
    {
        Objective objective = (Objective)message.Data;
        if (objective != null)
        {
            objectiveUIElementsDict[objective].UpdateProgress(objective.Progress);
            if (objective.IsComplete)
                objectiveUIElementsDict[objective].MarkAsComplete();
        }
    }

    private void OnTableLoaded (IMessage message)
    {
        ObjectiveUIElement[] elements = GetComponentsInChildren<ObjectiveUIElement>(true);
        for (int i = 0; i < elements.Length; i++)
        {
            Destroy(elements[i]);
        }
        objectiveUIElementsDict.Clear();
    }

}
