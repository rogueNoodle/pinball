﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillUI : MonoBehaviour {

    [SerializeField] private SkillSlot skillSlot;

    public SkillSlot SkillSlot
    {
        get
        {
            return skillSlot;
        }
    }

    [SerializeField] private Image icon;
    [SerializeField] private Image meter;

    Color color = new Color(1,1,1,0.2f);

    public float MeterValue {
        get
        {
            return meter.fillAmount;
        }
        set
        {
            meter.fillAmount = value;
        }
    }

    public void CooldownStart ()
    {
        icon.color = color;
        meter.fillAmount = 0;
    }

    public void CooldownComplete()
    {
        icon.color = Color.white;
        meter.fillAmount = 1f;
    }
}

public enum SkillSlot 
{
    Bottom,
    Left,
    Right,
    Top
};
