﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using RN.Pinball;

public class StaminaUI : MonoBehaviour
{

    [SerializeField] private Image staminaMeter;
    [SerializeField] private Image unconsciousnessMeter;
    private void LateUpdate ()
    {
        staminaMeter.fillAmount = Mathf.Lerp(0, 0.75f, Player.Instance.Stamina.Percentage);
        unconsciousnessMeter.fillAmount = Mathf.Lerp(0, 0.25f, Player.Instance.CurrentUnconsciousnessPercentage);
    }
}

