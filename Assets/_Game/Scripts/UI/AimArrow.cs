﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimArrow : MonoBehaviour {

    [SerializeField]
    private Transform target;

    [SerializeField]
    private Transform arrowhead;

    [SerializeField]
    private Transform arrowShaft;

    [SerializeField] 
    private Vector3 offset = new Vector3(0,0.125f,0);
    [SerializeField] 
    private LayerMask layerMask;

    private Vector3 newPosition = Vector3.zero;
    private Vector3 arrowshaftScale = new Vector3(1, 1, 1);
    private RaycastHit[] hits = new RaycastHit[5];


	void LateUpdate ()
    {
        newPosition.x = target.position.x;
        newPosition.y = target.position.y + offset.y;
        newPosition.z = target.position.z;
        transform.position = newPosition;

        Vector3 forward = PlayerInput.Aim;
        float magnitude = forward.sqrMagnitude;
        if (magnitude > 0)
        {
            transform.forward = forward;

            float distance = 1;
            if (Physics.RaycastNonAlloc(transform.position, transform.forward, hits, 1, layerMask) > 0)
            {
                distance = hits[0].distance;
            }

            arrowshaftScale.z = distance - 0.125f;
            arrowShaft.localScale = arrowshaftScale;
            arrowhead.localPosition = Vector3.forward * distance;
            arrowhead.localScale = Vector3.one;
        }
        else
        {
            arrowShaft.localScale = Vector3.zero;
            arrowhead.localPosition = Vector3.zero;
            arrowhead.localScale = Vector3.zero;
        }

	}
}
