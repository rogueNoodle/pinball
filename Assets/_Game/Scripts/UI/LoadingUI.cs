﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using com.ootii.Messages;

public class LoadingUI : MonoBehaviour
{
    [SerializeField] private Image background;
    [SerializeField] private TextMeshProUGUI loadingText;

    public IEnumerator FadeOut (float duration)
    {
        loadingText.DOFade(0, duration * 0.5f).SetUpdate(UpdateType.Normal, true).SetEase(Ease.InOutSine);
        Tweener backgroundFade = background.DOFade(0, duration).SetUpdate(UpdateType.Normal, true).SetEase(Ease.InOutSine).SetDelay(duration * 0.5f);
        yield return backgroundFade.WaitForCompletion();
        MessageDispatcher.SendMessage(SceneMessages.SceneFadeInComplete);
    }

    public IEnumerator FadeIn (float duration)
    {
        loadingText.alpha = 0;
        Color bgColor = background.color;
        bgColor.a = 0;
        background.color = bgColor;

        Tweener backgroundFade = background.DOFade(1, duration).SetUpdate(UpdateType.Normal, true).SetEase(Ease.InOutSine);
        loadingText.DOFade(1, duration * 0.25f).SetUpdate(UpdateType.Normal, true).SetEase(Ease.InOutSine).SetDelay(duration);
        yield return backgroundFade.WaitForCompletion();
    }

}
