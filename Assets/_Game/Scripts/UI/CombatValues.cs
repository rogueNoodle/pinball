﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using TMPro;

public class CombatValues : MonoBehaviour {

    public GameObject playerCombatValuePrefab;
    public GameObject playerDamageValuePrefab;

    private void Awake ()
    {
        MessageDispatcher.AddListener(ActorMessages.StatusChange, OnActorStatusChange);
    }

    private void OnDestroy ()
    {
        MessageDispatcher.RemoveListener(ActorMessages.StatusChange, OnActorStatusChange);
    }

    private void OnActorStatusChange (IMessage message)
    {
        StatusChangeData data = (StatusChangeData)message.Data;
        DisplayDamage(playerCombatValuePrefab, ref data);
    }

    private void DisplayDamage (GameObject prefab, ref StatusChangeData data)
    {
        var fwd = Camera.main.transform.forward;

        GameObject go = Instantiate(prefab, data.position, Quaternion.LookRotation(fwd), transform) as GameObject;
        TextMeshPro text = go.GetComponent<TextMeshPro>();
        text.color = data.color;
        text.text = data.text;
        text.ForceMeshUpdate();
    }


}
