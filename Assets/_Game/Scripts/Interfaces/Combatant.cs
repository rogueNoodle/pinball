﻿using UnityEngine;
using System.Collections;

public interface Combatant
{

    GameObject GameObject
    {
        get;
    }

    CombatantType CombatantType
    {
        get;
    }

    GameObject DebrisPrefab
    {
        get;
    }

    float BaseAttack
    {
        get;
    }

    float BaseDefense
    {
        get;
    }


    Stat AttackStat
    {
        get;
    }

    Stat DefenseStat
    {
        get;
    }

    Stat CriticalChanceStat
    {
        get;
    }

    Stat CriticalValueStat
    {
        get;
    }

    bool IsSpecialActive
    {
        get;
    }

    string SpecialAttack
    {
        get;
    }

    // Returns whether or not the combatant died
    bool ApplyDamage (float damage);
}

public enum CombatantType
{
    Player, // obvious
    PlayerAlly, // multiball or other player-allied object
    Enemy, // obvous
    Object // traps, crates, and other inanimate objects
};

