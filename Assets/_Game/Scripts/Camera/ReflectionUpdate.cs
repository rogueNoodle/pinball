﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReflectionUpdate : MonoBehaviour {

    [SerializeField] private float refreshRate = 0.1f;
    private WaitForSeconds refreshWait;
    private ReflectionProbe probe;

    private void Awake ()
    {
        probe = GetComponent<ReflectionProbe>();
        refreshWait = new WaitForSeconds(refreshRate);
    }

	// Use this for initialization
    IEnumerator Start () 
    {
        while (probe != null)
        {
            yield return refreshWait;
            probe.RenderProbe();
        }
        yield break;
	}


}
