﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour 
{
    
    [SerializeField] private Transform followTransform;
    [SerializeField] private Vector3 targetMinBounds;
    [SerializeField] private Vector3 targetMaxBounds;
    [SerializeField] private Vector3 followMinBounds;
    [SerializeField] private Vector3 followMaxBounds;
    [SerializeField] private GameObject followCameraPivot;
    [SerializeField] private GameObject staticCameraPivot;

    [SerializeField] private bool followCameraActive = false;

    private Vector3 TargetBoundsCenter
    {
        get
        {
            return Vector3.Lerp(targetMinBounds, targetMaxBounds, 0.5f);
        }
    }

    private Vector3 TargetBoundsSize
    {
        get
        {
            return targetMaxBounds - targetMinBounds;
        }
    }

    private Vector3 FollowBoundsCenter
    {
        get
        {
            return Vector3.Lerp(followMinBounds, followMaxBounds, 0.5f);
        }
    }

    private Vector3 FollowBoundsSize
    {
        get
        {
            return followMaxBounds - followMinBounds;
        }
    }

    private void OnDrawGizmos ()
    {
        Gizmos.DrawWireCube(TargetBoundsCenter, TargetBoundsSize);
        Gizmos.DrawWireCube(FollowBoundsCenter, FollowBoundsSize);
    }

    [SerializeField] private float smoothTime = 0.2f;
    [SerializeField] private float lookAheadScale = 0.2f;

    private Vector3 position = Vector3.zero;
    //private Vector3 targetPosition = Vector3.zero;
    private Vector3 velocity = Vector3.zero;

    private Rigidbody targetRigidbody;

    private Transform targetTransform;
    public void SetTarget (Transform target)
    {
        this.targetTransform = target;
        targetRigidbody = target.GetComponent<Rigidbody>();
    }

    private void Awake ()
    {
        followCameraPivot.SetActive(followCameraActive);
        staticCameraPivot.SetActive(!followCameraActive);
    }

    private void LateUpdate ()
    {
        if (PlayerInput.GetCameraTogglePressed())
        {
            followCameraActive = !followCameraActive;
            followCameraPivot.SetActive(followCameraActive);
            staticCameraPivot.SetActive(!followCameraActive);
        }
        if (targetTransform == null)
            return;

        // LERPED FOLLOW
        //targetPosition.x = Mathf.Lerp(followMinBounds.x, followMaxBounds.x, Mathf.InverseLerp(targetMinBounds.x, targetMaxBounds.x, targetTransform.position.x + targetRigidbody.velocity.x * lookAheadScale));
       // targetPosition.z = Mathf.Lerp(followMinBounds.z, followMaxBounds.z, Mathf.InverseLerp(targetMinBounds.z, targetMaxBounds.z, targetTransform.position.z + targetRigidbody.velocity.z * lookAheadScale));

        // SMOOTH FOLLOW
        position = Vector3.SmoothDamp(followTransform.position, targetTransform.position + (targetRigidbody.velocity * lookAheadScale), ref velocity, smoothTime);
        position.x = Mathf.Clamp(position.x, followMinBounds.x, followMaxBounds.x);
        position.y = Mathf.Clamp(position.y, followMinBounds.y, followMaxBounds.y);
        position.z = Mathf.Clamp(position.z, followMinBounds.z, followMaxBounds.z);
        followTransform.position = position;
    }
}
