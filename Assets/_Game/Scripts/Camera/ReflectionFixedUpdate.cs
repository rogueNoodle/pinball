﻿using UnityEngine;
using System.Collections;

public class ReflectionFixedUpdate : MonoBehaviour
{
    private ReflectionProbe probe;

    private void Awake ()
    {
        probe = GetComponent<ReflectionProbe>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        probe.RenderProbe();
    }
}

