﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(ObjectiveManager))]
public class ObjectiveManagerEditor : Editor {

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        DrawDefaultInspector();
        if (GUILayout.Button("Add Objective"))
        {
            ObjectiveManager manager = serializedObject.targetObject as ObjectiveManager;
            Debug.Log(manager);
            GameObject objectiveGO = new GameObject("New Objective");
            objectiveGO.AddComponent<Objective>();
            objectiveGO.transform.SetParent(manager.gameObject.transform);
            EditorSceneManager.MarkSceneDirty(objectiveGO.scene);
        }
    }
}
