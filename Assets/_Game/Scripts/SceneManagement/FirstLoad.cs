﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FirstLoad : MonoBehaviour {
    public string initialSceneName = "MainMenu";
	// Use this for initialization
	void Awake () {
        SceneController.Instance.LoadScene(initialSceneName);
	}
}
