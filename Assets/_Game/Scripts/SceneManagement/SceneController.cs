﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using com.ootii.Messages;
using Rewired;

public class SceneController : MonoBehaviour 
{
    #region SINGLETON

    private static SceneController instance;

    public static SceneController Instance
    {
        get
        {
            if (instance == null)
            {
                SceneController[] instances = FindObjectsOfType<SceneController>();
                if (instances.Length > 1)
                {
                    Debug.LogError("SceneController: More than one instance exists");
                }
                else if (instances.Length == 0)
                {
                    Debug.LogWarning("SceneController: No instance exists. Creating one, but this may not be expected");
                    GameObject sceneControllerGO = new GameObject("SceneController (Generated)", typeof(SceneController));
                    instance = sceneControllerGO.GetComponent<SceneController>();
                }
                else
                {
                    instance = instances[0];
                }
            }
            return instance;
        }
    }

    #endregion

    private LoadingUI loadingUI;
    public LoadingUI LoadingUI
    {
        get
        {
            if (loadingUI == null)
            {
                loadingUI = FindObjectOfType<LoadingUI>();
            }

            return loadingUI;
        }
    }

    private EventSystem eventSystem;
    public EventSystem EventSystem
    {
        get
        {
            if (eventSystem == null)
            {
                eventSystem = FindObjectOfType<EventSystem>();
            }
            return eventSystem;
        }
    }

    [SerializeField] private InputManager rewiredInputManager;

    public static void LoadRootFromScene (string sceneName)
    {

        // Creates root scene and instantiates root prefab

        Scene rootScene = SceneManager.GetSceneByName("Root");
        if (!rootScene.IsValid())
        {
            rootScene = SceneManager.CreateScene("Root");
            SceneManager.SetActiveScene(rootScene);
            GameObject.Instantiate(Resources.Load<GameObject>("Root"));

            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                Scene scene = SceneManager.GetSceneAt(i);
                if (scene.name != "Root")
                {
                    SceneManager.UnloadSceneAsync(scene);
                }
            }
        }
            
        //SceneController forceInstance = Instance;
        Instance.StartCoroutine(Instance.LoadSceneRoutine(sceneName));
    }

    private void Awake ()
    {
        MessageDispatcher.AddListener(SceneMessages.SceneLoadComplete, OnSceneLoadComplete);

    }

    private void OnDestroy ()
    {
        MessageDispatcher.RemoveListener(SceneMessages.SceneLoadComplete, OnSceneLoadComplete);
    }

    public void LoadScene (string sceneName)
    {
        StartCoroutine(LoadSceneRoutine(sceneName));
    }

    public IEnumerator LoadSceneRoutine (string loadSceneName)
    {

        // display loading screen & disable event system
        LoadingUI.gameObject.SetActive(true);
        yield return StartCoroutine(LoadingUI.FadeIn(0.5f));

        // unload all scenes except Root scene

        for (int i = SceneManager.sceneCount - 1; i >= 0; i--)
        {
            Scene scene = SceneManager.GetSceneAt(i);
            if (scene.name != "Root")
            {
                Debug.LogWarning("Unloading Scene : " + scene.name);
                AsyncOperation unloadAO = SceneManager.UnloadSceneAsync(scene);
                if (unloadAO != null)
                {
                    yield return new WaitUntil(() => unloadAO.isDone);
                }
            } 
        }
        yield return null;
        // Load requested scene and set active on load complete

        AsyncOperation loadAO = SceneManager.LoadSceneAsync(loadSceneName, LoadSceneMode.Additive);
        yield return new WaitUntil(() => loadAO.isDone);
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(loadSceneName));
    }

    /// <summary>
    /// Scene load complete is sent by scenes once they have completed their safe start initialization.
    /// Here, we use this to begin the fade out of the loading screen.
    /// Loading screen in turn sends fadeout complete message that is used by scenes to know that the scene is now completely visible and gameplay can begin
    /// </summary>
    /// <param name="message">Message.</param>
    private void OnSceneLoadComplete (IMessage message)
    {
        StartCoroutine(LoadingUI.FadeOut(1));
    }

}

