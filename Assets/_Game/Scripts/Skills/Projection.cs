﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projection : Skill {

    [SerializeField] private float lifespan = 3f;

    [SerializeField] private GameObject prefab;
    GameObject currentProjection;

    protected override void SkillActivated()
    {
        currentProjection = GameObject.Instantiate(prefab, transform.position, Quaternion.identity) as GameObject;
        Rigidbody prb = currentProjection.GetComponent<Rigidbody>();
        Ball pBall = currentProjection.GetComponent<Ball>();
        prb.velocity = Aim * pBall.MaxSpeed;
        StartCoroutine(Kill());
    }

    private IEnumerator Kill()
    {
        yield return new WaitForSeconds(lifespan);
        if (currentProjection != null)
        {
            Destroy(currentProjection);
        }
    }
	
}
