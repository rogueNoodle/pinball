﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slow : Skill {

    [SerializeField] private float timeScale;
    [SerializeField] private float duration;

    protected override void SkillActivated()
    {
        Time.timeScale = timeScale;

        StartCoroutine(Stop());
    }

    private IEnumerator Stop()
    {
        yield return new WaitForSeconds(duration);
        Time.timeScale = 1;
    }
}
