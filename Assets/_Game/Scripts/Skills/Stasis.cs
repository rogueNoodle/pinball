﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stasis : Skill
{
    Vector3 velocity;
    Vector3 angularVelocity;

	protected override void SkillActivated()
    {
        Debug.Log("Stasis Activated");
        velocity = Rigidbody.velocity;
        angularVelocity = Rigidbody.angularVelocity;
        Rigidbody.isKinematic = true;
    }

    protected override void SkillComplete()
    {
        Rigidbody.isKinematic = false;
        Rigidbody.angularVelocity = angularVelocity;
        Rigidbody.velocity = velocity;
    }
}
