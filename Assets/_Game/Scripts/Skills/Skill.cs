﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class Skill : MonoBehaviour {

    [SerializeField] private SkillUI skillUI;
    [SerializeField] private bool isOneShot = true;
    [SerializeField] private float skillMaximumDuration = 3f;
    private float skillCurrentDuration = 0f;

    [SerializeField] private float cooldownDuration = 4;
    [SerializeField] private bool requiresAim = false;
    private Vector3 aim = Vector3.zero;

    [SerializeField] private bool skillEnabled = false;

    public Vector3 Aim
    {
        get
        {
            return aim;
        }
    }

    public float CooldownDuration
    {
        get
        {
            return cooldownDuration;
        }
        set
        {
            cooldownDuration = value;
        }
    }

    private bool isCoolingDown = false;

    public bool IsCoolingDown
    {
        get
        {
            return isCoolingDown;
        }
        set
        {
            isCoolingDown = value;
        }
    }

    private bool isActive = false;

    public bool IsActive
    {
        get
        {
            return isActive;
        }
        set
        {
            isActive = value;
        }
    }



    private Rigidbody rb;

    public Rigidbody Rigidbody
    {
        get
        {
            return rb;
        }
        set
        {
            rb = value;
        }
    }

    private Ball ball;

    public Ball Ball
    {
        get
        {
            return ball;
        }
        set
        {
            ball = value;
        }
    }


    private void Awake ()
    {
        ball = GetComponent<Ball>();
        rb = GetComponent<Rigidbody>();
        MessageDispatcher.AddListener("DisableSkills", OnDisableSkills);
        MessageDispatcher.AddListener("EnableSkills", OnEnableSkills);
    }

    private void OnDestroy ()
    {
        MessageDispatcher.RemoveListener("DisableSkills", OnDisableSkills);
        MessageDispatcher.RemoveListener("EnableSkills", OnEnableSkills);
    }

    private void OnDisableSkills (IMessage message)
    {
        skillEnabled = false;
    }

    private void OnEnableSkills (IMessage message)
    {
        skillEnabled = true;
    }

    protected virtual void Update ()
    {
        skillUI.gameObject.SetActive(skillEnabled);
        if (isCoolingDown || isActive || !skillEnabled)
            return;

        if (isOneShot)
        {
            if (PlayerInput.GetSkillSlotDown(skillUI.SkillSlot))
            {
                if (requiresAim)
                {
                    aim = PlayerInput.Aim;
                    if (aim.sqrMagnitude == 0)
                        return;
                }
                SkillActivated();
                StartCoroutine(Cooldown());
            }
        }
        else
        {
            if (PlayerInput.GetSkillSlotPressed(skillUI.SkillSlot))
            {

                if (requiresAim)
                {
                    aim = PlayerInput.Aim;
                    if (aim.sqrMagnitude == 0)
                        return;
                }

                StartCoroutine(DurationSkill());
            }
        }
    }

    private IEnumerator Cooldown()
    {
        float progress = 0;
        isCoolingDown = true;
        skillUI.CooldownStart();
        while (progress < cooldownDuration)
        {
            yield return null;
            progress += Time.deltaTime;
            skillUI.MeterValue = progress / cooldownDuration;
        }
        skillUI.CooldownComplete();
        isCoolingDown = false;
    }

    private IEnumerator DurationSkill()
    {
        SkillActivated();
        skillCurrentDuration = 0;
        isActive = true;
        while (skillCurrentDuration < skillMaximumDuration && PlayerInput.GetSkillSlotPressed(skillUI.SkillSlot))
        {
            yield return null;
            skillCurrentDuration += Time.deltaTime;
            skillUI.MeterValue = 1 - (skillCurrentDuration / skillMaximumDuration);
            SkillUpdate();
        }
        yield return null;
        SkillComplete();
        isActive = false;
        StartCoroutine(Cooldown());
    }

    protected virtual void SkillActivated ()
    {

    }

    protected virtual void SkillUpdate ()
    {

    }

    protected virtual void SkillComplete ()
    {

    }
}
