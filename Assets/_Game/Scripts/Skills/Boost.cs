﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class Boost : Skill {

    

    protected override void SkillActivated()
    {


        Vector3 aim = PlayerInput.Aim;
        if (aim.sqrMagnitude > 0)
        {
            Rigidbody.velocity = aim * Ball.MaxSpeed;
        }
        else
        {
            Rigidbody.velocity = (Rigidbody.velocity.sqrMagnitude > 0 ? Rigidbody.velocity.normalized : transform.forward) * Ball.MaxSpeed;
        }

    }
}
