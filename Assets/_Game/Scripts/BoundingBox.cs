﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundingBox : MonoBehaviour {

    [SerializeField] private Vector3 size;
    [SerializeField] private Vector3 offset;

    private Vector3 Center
    {
        get
        {
            return new Vector3(offset.x, size.y * 0.5f + offset.y, size.z * 0.5f + offset.z);
        }
    }


    private void OnDrawGizmos ()
    {
        Gizmos.DrawWireCube(Center, size);
    }
}
