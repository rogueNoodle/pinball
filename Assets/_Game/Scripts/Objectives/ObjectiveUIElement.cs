﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class ObjectiveUIElement : MonoBehaviour
{

    private bool isPrimary = false;
    public bool IsPrimary
    {
        get
        {
            return isPrimary;
        }
        set
        {
            isPrimary = value;
            description.fontStyle = isPrimary ? TMPro.FontStyles.Bold : TMPro.FontStyles.Italic;
        }
    }

    [SerializeField] private TextMeshProUGUI description;
    public void SetDescription (string text)
    {
        description.text = text;
    }

    [SerializeField] private Image progressBar;
    public void UpdateProgress(float progress)
    {
        progressBar.fillAmount = progress;
    }

    public void MarkAsComplete ()
    {
        description.alpha = 0.3f;
        description.ForceMeshUpdate();
    }
}

