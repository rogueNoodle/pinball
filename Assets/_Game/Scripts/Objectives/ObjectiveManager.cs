﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.ootii.Messages;
using UnityEngine.UI;
using TMPro;
public class ObjectiveManager : MonoBehaviour
{
    [SerializeField] UnityEvent onPrimaryObjectivesComplete;
    [SerializeField] UnityEvent onSecondaryObjectivesComplete;

   
    private List<Objective> objectives = new List<Objective>();

    private int primaryObjectiveCount = 0;
    private int secondaryObjectiveCount = 0;

    private void Awake ()
    {
        MessageDispatcher.SendMessage(GameMessages.TableLoaded, 0);
        for (int i = 0; i < transform.childCount; i++)
        {
            Objective obj = transform.GetChild(i).GetComponent<Objective>();
            if (obj != null)
            {
                obj.Manager = this;

                if (obj.IsPrimary)
                    primaryObjectiveCount++;
                else
                    secondaryObjectiveCount++;
                
                objectives.Add(obj);
            }
        }
    }

    private void Start ()
    {
        for (int i = 0; i < objectives.Count; i++)
        {
            MessageDispatcher.SendMessage(this, ObjectiveMessages.ObjectiveAdded, objectives[i], 1.5f + (i * 0.125f));
        }
    }



    public void ObjectiveUpdated (Objective objective)
    {
        MessageDispatcher.SendMessage(this, ObjectiveMessages.ObjectivesUpdated, objective, 0);

        if (objective.IsPrimary && objective.IsComplete)
        {
            primaryObjectiveCount--;
            if (primaryObjectiveCount == 0)
                onPrimaryObjectivesComplete.Invoke();
        }
        else if (objective.IsComplete)
        {
            secondaryObjectiveCount--;
            if (secondaryObjectiveCount == 0)
                onSecondaryObjectivesComplete.Invoke();
        }

    }
}


