﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
public class Objective : MonoBehaviour {

    private ObjectiveManager manager;

    public ObjectiveManager Manager
    {
        get
        {
            return manager;
        }
        set
        {
            manager = value;
        }
    }

    [SerializeField] private bool isPrimary = false;
    public bool IsPrimary
    {
        get
        {
            return isPrimary;
        }
    }

    [SerializeField] private int count;

    public int Count
    {
        get
        {
            return count;
        }
    }

    private int currentCount;

    public int CurrentCount
    {
        get
        {
            return currentCount;
        }
    }

    public bool IsComplete
    {
        get {
            return currentCount >= count;
        }
    }

    public void Increment ()
    {
        currentCount++;
        if (Manager != null)
        {
            Manager.ObjectiveUpdated(this);
        }
    }

    public float Progress {

        get {
            return Mathf.Clamp((float)currentCount / (float)count, 0, 1);
        }
    }

}
