﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[SelectionBase]
public class Collectible : MonoBehaviour
{

    [SerializeField] private UnityEvent onCollect;
	private void OnTriggerEnter (Collider other)
    {
        onCollect.Invoke();
        gameObject.SetActive(false);
    }


}
