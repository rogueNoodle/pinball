﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour {

    [Tooltip("'LeftFlipper' or 'RightFlipper'")]
    [SerializeField] private string flipperSide = "LeftFlipper";
    [SerializeField] private float targetVelocity = 3000;
    [SerializeField] private float force = 1500;
    [SerializeField] private Vector3 returnForce = new Vector3(0, 0, -1000);
    private static string sound = "flipper";

    [Tooltip("Hinge can be located on a child of the flipper root gameobject")]
    private HingeJoint hinge;
    private Rigidbody _rigidbody;
    private bool isPressed = false;

    private void Awake ()
    {
        Configure();
    }

    private void Configure ()
    {
        hinge = GetComponentInChildren<HingeJoint>(true);
        JointMotor motor = hinge.motor;
        motor.targetVelocity = targetVelocity;
        motor.force = force;
        hinge.motor = motor;
        hinge.useMotor = false;

        ConstantForce constantForce = GetComponentInChildren<ConstantForce>(true);
        if (constantForce != null)
        {
            constantForce.force = returnForce;
        }
    }

    public Rigidbody Rigidbody
    {
        get
        {
            if (_rigidbody == null)
            {
                _rigidbody = GetComponentInChildren<Rigidbody>(true);
            }
            return _rigidbody;
        }
    }


    private void Update ()
    {
        if (PlayerInput.GetFlipper(flipperSide) && !isPressed)
        {
            isPressed = true;
            hinge.useMotor = true;
            AudioController.Play(sound, transform.position);
        }
        else if (!PlayerInput.GetFlipper(flipperSide) && isPressed)
        {
            isPressed = false;
            hinge.useMotor = false;
        }
    }

}
