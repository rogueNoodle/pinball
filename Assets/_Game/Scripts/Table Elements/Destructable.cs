﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructable : MonoBehaviour {

    [SerializeField]
    private float health = 3;

    [SerializeField]
    private GameObject debrisPrefab;

    [SerializeField]
    private float explosionForce = 10f;

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.collider.CompareTag("Ball"))
            return;

        health--;
        if (health <= 0)
        {
            GameObject debris = Instantiate(debrisPrefab, transform.position, transform.rotation) as GameObject;
            foreach (Rigidbody rb in debris.GetComponentsInChildren<Rigidbody>(true))
            {
                rb.AddExplosionForce(explosionForce * collision.relativeVelocity.magnitude, collision.contacts[0].point, 4, 0.1f);
                rb.AddTorque(rb.transform.right * 100000, ForceMode.Impulse);
            }
            Destroy(gameObject);
        }
    }
}
