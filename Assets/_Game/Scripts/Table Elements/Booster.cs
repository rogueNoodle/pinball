﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Booster : MonoBehaviour {

    private void OnTriggerEnter (Collider other)
    {
        Ball ball = other.GetComponent<Ball>();
        if (ball != null)
        {
            Vector3 ballVelocity = ball.Rigidbody.velocity;
            if (Vector3.Dot(ballVelocity, transform.forward) > 0)
            {
                ball.Rigidbody.velocity = transform.forward * ball.MaxSpeed;
            }
        }
    }
}
