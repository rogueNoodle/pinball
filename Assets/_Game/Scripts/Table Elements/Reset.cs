﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;
using RN.Framework;

public class Reset : SafeBehaviour
{
    [SerializeField] private float resetHeight = -1;

    private Vector3 position;

    public Vector3 Position
    {
        get
        {
            Debug.Log("Get Spawn Position : " + position.ToString());
            return position;
        }
        set
        {
            Debug.Log("Set Spawn Position : " + position.ToString());
            position = value;
        }
    }

    private Quaternion rotation;
    private Ball ball;

    private void Awake ()
    {
        ball = GetComponent<Ball>();
        rotation = transform.rotation;
    }


    private void FixedUpdate ()
    {
        if (transform.position.y < resetHeight)
        {
            MessageDispatcher.SendMessage(PlayerMessages.RingOut);
           PerformReset();
        }
    }

    public void PerformReset ()
    {
        StartCoroutine(PerformResetRoutine());
    }

    private IEnumerator PerformResetRoutine ()
    {
        ball.ConstantForce.enabled = false;
        ball.Rigidbody.useGravity = false;
        transform.position = Position;
        transform.rotation = rotation;
        yield return new WaitForEndOfFrame();
        ball.Rigidbody.velocity = Vector3.zero;
        ball.Rigidbody.angularVelocity = Vector3.zero;
        yield return new WaitUntil(() => ball.Rigidbody.velocity.sqrMagnitude > 0.1f);
        ball.Rigidbody.useGravity = true;
        ball.ConstantForce.enabled = true;
    }
}

