﻿using UnityEngine;
using System.Collections;

public class HealingSource : MonoBehaviour
{

    [SerializeField] private float minPercentage = 0;
    [SerializeField] private float maxPercentage = 0.05f;
    private void OnCollisionEnter (Collision collision)
    {
        PlayerHealth health = collision.gameObject.GetComponent<PlayerHealth>();
        if (health != null)
        {
            health.ApplyHealingPercentage(maxPercentage);
        }
    }
}

