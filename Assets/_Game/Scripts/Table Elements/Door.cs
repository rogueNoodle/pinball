﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

    [SerializeField] private bool autoClose = false;
    [SerializeField] private float autoCloseDelay = 2f;
    [SerializeField] private string openAudioClipName;
    [SerializeField] private string closeAudioClipName;
    private WaitForSeconds waitForAutoCloseDelay;
    private Animator animator;

    private void Awake ()
    {
        animator = GetComponent<Animator>();
    }

    public void Open()
    {
        StopAllCoroutines();
        if (animator.GetBool("IsOpen"))
            return;
        
        animator.SetBool("IsOpen", true);
        if (!string.IsNullOrEmpty(openAudioClipName))
        {
            AudioController.Play(openAudioClipName, transform.position);
        }
        if (autoClose)
        {
            StartCoroutine(AutoClose());
        }

               
    }

    public void Close()
    {
        StopAllCoroutines();
        if (!animator.GetBool("IsOpen"))
            return;
        
        animator.SetBool("IsOpen", false);
        if (!string.IsNullOrEmpty(closeAudioClipName))
        {
            AudioController.Play(closeAudioClipName, transform.position);
        }
    }

    public void Toggle()
    {
        if (animator.GetBool("IsOpen"))
        {
            Close();
        }
        else
        {
            Open();
        }
    }

    private IEnumerator AutoClose ()
    {
        if (waitForAutoCloseDelay == null)
        {
            waitForAutoCloseDelay = new WaitForSeconds(autoCloseDelay);
        }
        yield return waitForAutoCloseDelay;
        Close();
    }
}
