﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour 
{
    [SerializeField] private float force = 100;
    private Vector3 collidingObjectPosition = Vector3.zero;
    private Vector3 thisPosition = Vector3.zero;
    private Vector3 difference = Vector3.zero;

    public float Force
    {
        get
        {
            return force;
        }
        set
        {
            force = value;
        }
    }

    private void OnCollisionEnter (Collision collision)
    {
        if (collision.collider.CompareTag("Ball"))
        {
            StartCoroutine(CheckForBump(collision));
        }
    }

    private static WaitForEndOfFrame endOfFrame = new WaitForEndOfFrame();
    private IEnumerator CheckForBump(Collision collision)
    {
        yield return endOfFrame;
        if (collision.gameObject != null)
        {
            Bump(collision.rigidbody, collision.transform);
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Debug.Log("HIT");
        Bump(hit.rigidbody, hit.transform);
    }

    private void Bump (Rigidbody rb, Transform tf)
    {
        thisPosition.x = transform.position.x;
        thisPosition.z = transform.position.z;
        collidingObjectPosition.x = tf.position.x;
        collidingObjectPosition.z = tf.position.z;
        difference.x = collidingObjectPosition.x - thisPosition.x;
        difference.z = collidingObjectPosition.z - thisPosition.z;
        difference.Normalize();
        rb.AddForce(difference * force, ForceMode.Impulse);
    }
}
