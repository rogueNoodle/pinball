﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class Plunger : MonoBehaviour {

    [SerializeField] private GameObject meterGroup;
    [SerializeField] private SpriteRenderer[] spriteRenderers;
    [Range(0,10)][SerializeField] private float strength = 5f;
    [SerializeField] private float meterIncreaseRate = 5f;
    [SerializeField] private float force = 500;
    [SerializeField] private bool forceBased = true;

    private Color partialColor = Color.white;
    private float previousStrength = -1;
    private int previousFullStrength = -1;
    private Ball ball;

    private void OnTriggerEnter (Collider collider)
    {
        ball = collider.GetComponent<Ball>();
        meterGroup.SetActive(true);
        MessageDispatcher.SendMessage("DisableSkills");
    }

    private void OnTriggerExit (Collider collider)
    {
        ball = null;
        meterGroup.SetActive(false);
        MessageDispatcher.SendMessage("EnableSkills");
    }
	
	void Update ()
    {
        // there is no ball in the plunger area
        if (ball == null)
            return;

        // Plunger is activated by the player's bottom skill button
        if (PlayerInput.GetSkillSlotDown(SkillSlot.Bottom))
        {
            // if force based, apply force. Otherwise, plunger strength is percentage of ball max speed
            if (forceBased)
                ball.Rigidbody.AddForce(transform.forward * force * (strength * .1f));
            else
                ball.Rigidbody.velocity = transform.forward * ball.MaxSpeed * (strength * 0.1f);
        }

        // Flippers are used to increase/decrease strength
        float strengthInputDelta = (PlayerInput.GetFlipper("LeftFlipper") ? -1 : 0) + (PlayerInput.GetFlipper("RightFlipper") ? 1 : 0) ;

        // no change in strength via input
        if (strengthInputDelta == 0)
            return;
        
        // Adjust strength based on input and meter increase rate
        strength += strengthInputDelta * meterIncreaseRate * Time.deltaTime;

        // Clamp
        strength = Mathf.Clamp(strength, 0, 10);

        // if previous == current, return

        if (previousStrength == strength)
            return;

        previousStrength = strength;
        int fullStrength = Mathf.FloorToInt(strength);
        if (fullStrength != previousFullStrength)
        {
            previousFullStrength = fullStrength;
            for (int i = 0; i < spriteRenderers.Length; i++)
            {
                spriteRenderers[i].color = i < fullStrength ? Color.white : Color.clear;
            }
        }

        float remainder = strength - fullStrength;
        if (remainder >= 0)
        {
            int partial = fullStrength;
            if (partial < spriteRenderers.Length && partial >= 0)
            {
                partialColor.a = remainder;
                spriteRenderers[partial].color = partialColor;
            }
        }
	}
}
