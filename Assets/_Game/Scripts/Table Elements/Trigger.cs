﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Trigger : MonoBehaviour {

    public UnityEvent onTrigger;
    public UnityEvent onTriggerExit;

    [SerializeField] private bool triggerDuringStay = false;
    [SerializeField] private float triggerRate = 1f;
    int insideCount = 0;

    private WaitForSeconds waitForTriggerRate;

    public WaitForSeconds WaitForTriggerRate
    {
        get
        {
            if (waitForTriggerRate == null)
            {
                waitForTriggerRate = new WaitForSeconds(triggerRate);
            }
            return waitForTriggerRate;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        onTrigger.Invoke();
    }

    private IEnumerator TriggerDuringStay ()
    {
        while (true)
        {
            yield return WaitForTriggerRate;
            onTrigger.Invoke();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            insideCount++;
            onTrigger.Invoke();
            if (triggerDuringStay)
            {
                StartCoroutine(TriggerDuringStay());
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        
        if (other.CompareTag("Ball"))
        {
            insideCount--;
            if (insideCount == 0)
            {
                StopAllCoroutines();
            }
            onTriggerExit.Invoke();
        }

    }
}
