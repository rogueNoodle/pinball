﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debris : MonoBehaviour {

    [SerializeField]
    private float explosionForce = 25;

    private Vector3 explosionPosition;
    public Vector3 ExplosionPosition
    {
        get
        {
            return explosionPosition;
        }

        set
        {
            explosionPosition = value;
        }
    }

    private float force;
    public float Force
    {
        get
        {
            return force;
        }
        set
        {
            force = value;
        }
    }
    IEnumerator Start()
    {
        foreach(Rigidbody rb in GetComponentsInChildren<Rigidbody>(true))
        {
            rb.AddExplosionForce(explosionForce * Force, ExplosionPosition, 4, 0.1f);
            rb.AddTorque(rb.transform.right * 1000, ForceMode.Impulse);
        }

        yield return new WaitForSeconds(6);
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
            yield return new WaitForSeconds(0.25f);
        }

        Destroy(gameObject);
    }
}
