﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Attackable : MonoBehaviour {

    private bool isDead = false;

    [SerializeField]
    private float maxHealth;
    private float currentHealth;

    [SerializeField]
    private float attackDamage = 0;

    [SerializeField]
    private GameObject deathPrefab;

    [SerializeField]
    private UnityEvent onKilled;

    public bool IsDead
    {
        get
        {
            return isDead;
        }

        set
        {
            isDead = value;
        }
    }

    private void Awake ()
    {
        currentHealth = maxHealth;
    }

    private static AttackResults attackResults = new AttackResults();
    public AttackResults Attack (float damage, Collision collision = null)
    {
        currentHealth -= damage;

        attackResults.DidKill = currentHealth <= 0;
        attackResults.ReturnedDamage = attackResults.DidKill ? 0 : attackDamage;

        if (attackResults.DidKill)
        {
           
            GameObject debrisGO = Instantiate(deathPrefab, transform.position, transform.rotation) as GameObject;
            Debris debris = debrisGO.GetComponent<Debris>();
            if (debris != null)
            {
                debris.Force = collision.relativeVelocity.magnitude;
                debris.ExplosionPosition = collision.contacts[0].point;
            }
            isDead = true;
            onKilled.Invoke();
            Destroy(gameObject);
        }
        return attackResults;
    }
}

public struct AttackResults
{
    public bool DidKill;
    public float ReturnedDamage;
}
