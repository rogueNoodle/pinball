﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popper : MonoBehaviour {

    [SerializeField] private float frequency = 2f;
    [SerializeField] private float maxY = .5f;
    [SerializeField] bool activeAtStart = true;
    [SerializeField] private float force = 500;
    [SerializeField] private bool oneShot = false;
    private bool activated = false;
    Rigidbody _rigidbody;
	// Use this for initialization
	void Awake ()
    {
        _rigidbody = GetComponentInChildren<Rigidbody>();
	}

    private void Start ()
    {
        if (activeAtStart)
        {
            Activate();
        }
    }

    public void Activate ()
    {
        if (!activated)
        {
            activated = true;
            StartCoroutine(Pop());
        }
    }

    private IEnumerator Pop ()
    {
        while (activated)
        {
            reset = false;
            _rigidbody.AddForce(Vector3.up * force, ForceMode.Impulse);
            if (oneShot)
            {
                activated = false;
                yield break;
            }

            yield return new WaitForSeconds(frequency);
        }
    }


    public void Deactivate ()
    {
        StopAllCoroutines();
        activated = false;
    }

    bool reset = false;
    	
	// Update is called once per frame
	void FixedUpdate () {
        if (_rigidbody.transform.localPosition.y >= maxY && !reset)
        {
            reset = true;
            _rigidbody.velocity = Vector3.zero;
        }
	}
}
