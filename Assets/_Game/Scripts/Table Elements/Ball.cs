﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Rigidbody))]
public class Ball : MonoBehaviour {

    [SerializeField] private LayerMask sphereCastMask;

    [SerializeField] private float maxSpeed = 8f;
    public float MaxSpeed
    {
        get
        {
            return maxSpeed;
        }
    }

    public bool AtMaxSpeed
    {
        get
        {
            return Rigidbody.velocity.sqrMagnitude >= maxSpeedSqr;
        }
    }
        

    private float maxSpeedSqr;

    private Rigidbody _rigidbody;
    private Vector3 startPosition;
    private Quaternion startRotation;
    private new ConstantForce constantForce;

    public ConstantForce ConstantForce
    {
        get
        {
            if (constantForce == null)
            {
                constantForce = GetComponent<ConstantForce>();
            }
            return constantForce;
        }
    }

    private void Awake ()
    {
        maxSpeedSqr = maxSpeed * maxSpeed;
    }


    public Rigidbody Rigidbody
    {
        get {
            if (_rigidbody == null)
            {
                _rigidbody = GetComponent<Rigidbody>();
            }
            return _rigidbody;
        }
    }
    //
    //    private void OnDrawGizmos ()
    //    {
    //        Gizmos.color = Color.green;
    //        Gizmos.DrawWireSphere(transform.position + Rigidbody.velocity * Time.fixedDeltaTime, 0.125f);    
    //    }
    //

    private Vector3 lastVelocity;
    private Vector3 lastAngularVelocity;
    private Vector3 lastPosition;
    private void FixedUpdate ()
    {
        ClampVelocity();

        lastVelocity = Rigidbody.velocity;
        lastAngularVelocity = Rigidbody.angularVelocity;
        lastPosition = transform.position;
    }

    public void MaintainPhysics ()
    {
        Rigidbody.velocity = lastVelocity;
        Rigidbody.angularVelocity = lastAngularVelocity;
        transform.position = lastPosition;
    }

    private void ClampVelocity ()
    {
        // Clamp Velocity
        if (Rigidbody.velocity.sqrMagnitude > maxSpeedSqr)
        {
            Rigidbody.velocity = Vector3.ClampMagnitude(Rigidbody.velocity, maxSpeed);
        }
    }



}
