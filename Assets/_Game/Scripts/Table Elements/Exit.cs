﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class Exit : MonoBehaviour {

    private void OnTriggerEnter (Collider collider)
    {
        if (collider.CompareTag("Ball"))
        {
            collider.GetComponent<Ball>().Rigidbody.isKinematic = true;
        }

        MessageDispatcher.SendMessage(GameMessages.TableComplete, 0.5f);
    }
}
