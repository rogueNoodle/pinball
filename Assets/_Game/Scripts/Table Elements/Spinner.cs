﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour {

    [SerializeField] private float speed = 90f;
    Rigidbody _rigidbody;
	// Use this for initialization
	void Awake () {
        _rigidbody = GetComponent<Rigidbody>();
	}
//	
//	// Update is called once per frame
	void FixedUpdate () {
        Quaternion deltaRot = Quaternion.Euler(Vector3.up * speed * Time.deltaTime);
        _rigidbody.MoveRotation(deltaRot * _rigidbody.rotation);
	}
}
    