﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RN.Framework;
using com.ootii.Messages;

public class MainMenuController : RN.Framework.SceneBase {

    [SerializeField] private GameObject buttonGroup;
    [SerializeField] private Button newGameButton;
    [SerializeField] private Button continueButton;
    [SerializeField] private Button optionsButton;

    [SerializeField] private Animator uiAnimatorController;

    protected override void Awake()
    {
        base.Awake();
        buttonGroup.SetActive(false);
    }

    protected override void AddListeners()
    {
        MessageDispatcher.AddListener(SceneMessages.SceneFadeInComplete, OnSceneFadeInComplete);
    }

    protected override void RemoveListeners()
    {
        MessageDispatcher.RemoveListener(SceneMessages.SceneFadeInComplete, OnSceneFadeInComplete);
    }
  
    protected override void OnSceneFadeInComplete(IMessage message)
    {
        buttonGroup.SetActive(true);
        newGameButton.Select();
    }

    public void OnNewGameButtonPressed ()
    {
        buttonGroup.SetActive(false);
        SceneController.Instance.LoadScene("Table");
    }
}
