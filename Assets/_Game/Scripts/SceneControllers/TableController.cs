﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using RN.Framework;
using com.ootii.Messages;

public class TableController : RN.Framework.SceneBase {

    [SerializeField] private PlayerEntity playerEntity;
    [SerializeField] private GameObject followCamPrefab;
    [Tooltip ("If true, table will load specified Table and configuration. Editor only")]
    [SerializeField] private bool tableTesting = false;
    [SerializeField] private Table table;
    [SerializeField] private int testConfigurationIndex = 0;


    private string currentConfigurationName;

    protected override void Awake()
    {
        base.Awake();
        MessageDispatcher.AddListener(GameMessages.TableComplete, OnTableComplete);
    }

    protected override void OnDestroy()
    {
        MessageDispatcher.RemoveListener(GameMessages.TableComplete, OnTableComplete);
    }

    protected override IEnumerator SafeStart()
    {
        
        yield return StartCoroutine(LoadTable());

        InitializePlayerEntity();

        // Initialize UI with player info
        yield return null;
        AudioController.PlayAmbienceSound(table.ambientSoundName);
    }

    private IEnumerator LoadTable ()
    {
        // Load and merge  random config scene
        currentConfigurationName = table.RandomConfiguration();

        #if UNITY_EDITOR
        if (tableTesting)
        {
            if (testConfigurationIndex >= 0 && testConfigurationIndex < table.configurationSceneNames.Count)
            {
                currentConfigurationName = table.configurationSceneNames[testConfigurationIndex];
            }
        }
        #endif

        // Load and merge base table scene 
        AsyncOperation baseAO = SceneManager.LoadSceneAsync(table.baseSceneName, LoadSceneMode.Additive);
        AsyncOperation configAO = SceneManager.LoadSceneAsync(currentConfigurationName, LoadSceneMode.Additive);

        // wawit for both scenes to load
        yield return new WaitUntil(() => baseAO.isDone && configAO.isDone);

        SceneManager.MergeScenes(SceneManager.GetSceneByName(table.baseSceneName), gameObject.scene);
        SceneManager.MergeScenes(SceneManager.GetSceneByName(currentConfigurationName), gameObject.scene);

        yield break;
    }

    private void InitializePlayerEntity ()
    {
        // Spawn ball and initialize appearance
        GameObject spawnPoint = GameObject.FindWithTag("PlayerSpawnPoint");

        if (spawnPoint != null)
        {
            playerEntity.transform.position = spawnPoint.transform.position;
            Debug.Log(playerEntity.transform.position);
            Debug.Log(playerEntity.Ball.transform.position);
            playerEntity.Reset.Position = playerEntity.Ball.transform.position;
        }
        else
        {
            Debug.LogError("SpawnPoint missing in " + currentConfigurationName);
        }

        CameraController cameraController = FindObjectOfType<CameraController>();
        if (cameraController != null)
        {
            cameraController.SetTarget(playerEntity.Ball.transform);
        }
        else
        {
            Debug.LogError("CameraController not found");
        }

        playerEntity.Reset.PerformReset();
    }

    private void OnTableComplete (IMessage message)
    {
        SceneController.Instance.LoadScene("MainMenu");
    }
}
