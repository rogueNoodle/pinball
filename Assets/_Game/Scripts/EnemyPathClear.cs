﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPathClear : MonoBehaviour {

    [SerializeField]
    private  LayerMask layermask;

    [SerializeField]
    private float pushForce = 250;

    private static RaycastHit[] hits = new RaycastHit[3];


        
    private void Update ()
    {
        int hitCount = Physics.RaycastNonAlloc(transform.position, transform.forward, hits, 0.25f, layermask);
        //Debug.DrawRay(transform.position, transform.forward, Color.red);

        //Debug.Log("HitCount " + hitCount);
        if (hitCount > 0)
        {
            for (int i = 0; i < hitCount; i++)
            {
                hits[i].rigidbody.AddExplosionForce(pushForce * hits[i].rigidbody.mass, transform.position, 2);
            }
        }
    } 
}
