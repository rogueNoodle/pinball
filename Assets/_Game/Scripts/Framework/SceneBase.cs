﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

namespace RN.Framework
{
    public class SceneBase : SafeBehaviour 
    {

        protected virtual void Awake ()
        {
            AddListeners();
        }

        protected virtual void OnDestroy ()
        {
            RemoveListeners();
        }

        protected virtual void AddListeners ()
        {

        }

        protected virtual void RemoveListeners ()
        {

        }

        protected virtual void OnSceneFadeInComplete (IMessage message)
        {

        }

        protected override IEnumerator SafeStart()
        {
            yield break;
        }

        protected override void OnSafeStartComplete()
        {
            MessageDispatcher.SendMessage(SceneMessages.SceneLoadComplete, 0.1f);
        }
    }
}

