﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

namespace RN.Framework
{

    /// <summary>
    /// MonoBehaviour replacement base class. Provides safe update and start callbacks.
    /// </summary>
    /// 
  
    public class SafeBehaviour : MonoBehaviour
    {
        private static WaitUntil waitUntilRootIsLoaded = null;

        private IEnumerator Start ()
        {
            if (waitUntilRootIsLoaded == null)
            {
                waitUntilRootIsLoaded = new WaitUntil(() => Root.IsLoaded);
            }
            yield return waitUntilRootIsLoaded;
            yield return StartCoroutine(SafeStart());
            OnSafeStartComplete();
        }

        /// <summary>
        /// Start function replacement guaranteed to take place following 
        /// Root scene load.
        /// </summary>
        protected virtual IEnumerator SafeStart ()
        {
            yield break;
        }

        protected virtual void OnSafeStartComplete ()
        {

        }
            
    }
}