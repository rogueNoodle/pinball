﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace RN.Framework
{
    public static class Root
    {
        private static bool isLoading = false;

        private static Scene scene;
        public static Scene Scene
        {
            get
            {
                
                if (!scene.IsValid())
                {
                    scene = SceneManager.GetSceneByName("Root");
                    if (!isLoading && !scene.isLoaded)
                    {
                        isLoading = true;
                        SceneManager.LoadScene("Root", LoadSceneMode.Additive);
                    }
                }
                return scene;
            }
        }

        public static bool IsLoaded
        {
            get
            {
                return Scene.isLoaded;
            }
        }
    }
}


