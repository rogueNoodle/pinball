﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public static class CombatResolution {

    public static void PlayerVSCombatantCollision(PlayerCombatant player, Combatant enemyCombatant, Collision collision)
    {
        // neither combatant is player or player ally
        // Should never happen but worth checking
        if (player.CombatantType != CombatantType.Player && player.CombatantType != CombatantType.PlayerAlly && enemyCombatant.CombatantType != CombatantType.Player && enemyCombatant.CombatantType != CombatantType.PlayerAlly)
        {
            return;
        }

        // if both combatants are players or player allies
        if ((player.CombatantType == CombatantType.Player || player.CombatantType == CombatantType.PlayerAlly) && (enemyCombatant.CombatantType == CombatantType.Player || enemyCombatant.CombatantType == CombatantType.PlayerAlly))
        {
            return;
        }
        float dot = Vector3.Dot(collision.contacts[0].normal, collision.transform.forward);

        float enemyAttackAngleModifier = 1;

        if (dot <= -0.375f)
        {
            Debug.Log("BACKSTAB! 2x DAMAGE");
            enemyAttackAngleModifier = 0.25f;
        }
        else if (dot > -0.375f && dot <= 0.375f)
        {
            Debug.Log("STANDARD ATTACK... 1x DAMAGE");
            enemyAttackAngleModifier = 1;
        }
        else if (dot > 0.375f)
        {
            Debug.Log("HEAD-ON ATTACK :( 0.75x DAMAGE");
            enemyAttackAngleModifier = 1.5f;
        }

        float attackSpeed = collision.relativeVelocity.magnitude;
        float attackSpeedPercentage = attackSpeed / player.Ball.MaxSpeed;
        float playerAttack = Mathf.Max((player.BaseAttack * attackSpeedPercentage) - (enemyCombatant.BaseDefense * enemyAttackAngleModifier), 0);
        bool enemyKilled = enemyCombatant.ApplyDamage(playerAttack);

        AttackDetails details = new AttackDetails();
        details.location = collision.transform.position;
        details.value = playerAttack;
        MessageDispatcher.SendMessage(player, CombatMessage.PlayerAttacked, details, -1);

        // If the enemy wasn't killed
        if (!enemyKilled)
        {
            float enemyAttack = Mathf.Max((enemyCombatant.BaseAttack * enemyAttackAngleModifier) - player.BaseDefense, 0);
            Debug.LogFormat("{0} attacks for {1} damage", enemyCombatant.GameObject.name, enemyAttack);
            player.ApplyDamage(enemyAttack);
            details.location = player.transform.position;
            details.value = enemyAttack;
            MessageDispatcher.SendMessage(player, CombatMessage.PlayerReceivedDamage, details, -1);
        }
        else
        {
            if (enemyCombatant.DebrisPrefab != null)
            {
                GameObject debrisGO = GameObject.Instantiate(enemyCombatant.DebrisPrefab, enemyCombatant.GameObject.transform.position, enemyCombatant.GameObject.transform.rotation) as GameObject;
                Debris debris = debrisGO.GetComponent<Debris>();
                if (debris != null)
                {
                    debris.Force = attackSpeed;
                    debris.ExplosionPosition = collision.contacts[0].point;
                }
            }
            GameObject.Destroy(enemyCombatant.GameObject);
            player.Ball.MaintainPhysics();
        }
    }

    public static void BallVSEnemyCollision(PlayerCombatant player, Ball ball, EnemyCombatant enemy, Collision collision)
    {
        float attackSpeed = collision.relativeVelocity.magnitude;
        float attackSpeedPercentage = attackSpeed / ball.MaxSpeed;
        float dot = Vector3.Dot(collision.contacts[0].normal, collision.transform.forward);

        // Modifies enemy defense & attack
        float attackAngleModifier = 1;
        if (dot <= -0.375f)
        {
            Debug.Log("BACKSTAB! 2x DAMAGE");
            attackAngleModifier = 0;
        }
        else if (dot > -0.375f && dot <= 0.375f)
        {
            Debug.Log("STANDARD ATTACK... 1x DAMAGE");
            attackAngleModifier = 1;
        }
        else if (dot > 0.375f)
        {
            Debug.Log("HEAD-ON ATTACK :( 0.75x DAMAGE");
            attackAngleModifier = 2;
        }



    }

    public static void CombatantVSCombatantRangedCombat(Combatant attacker, Combatant target, string rangedSkill)
    {

    }


}
