﻿using UnityEngine;
using System.Collections;
using RN.Pinball;

public class PlayerCombatant : MonoBehaviour, Combatant
{
   
    private Ball ball;
    public Ball Ball
    {
        get
        {
            if (ball == null)
            {
                ball = GetComponent<Ball>();
            }
            return ball;
        }
    }
   

    #region Combatant implementation

    public GameObject DebrisPrefab
    {
        get
        {
            return null;
        }
    }

    public GameObject GameObject
    {
        get
        {
            return gameObject;
        }
    }

    public float BaseAttack
    {
        get
        {
            return Player.Instance.BaseAttack;
        }
    }

    public float BaseDefense
    {
        get
        {
            return Player.Instance.BaseDefense;
        }
    }

    public bool ApplyDamage(float damage)
    {
        return Player.Instance.ApplyDamage(damage);
    }

    public CombatantType CombatantType
    {
        get
        {
            return CombatantType.Player;
        }
    }

    public Stat AttackStat
    {
        get
        {
            return Player.Instance.AttackStat;
        }
    }

    public Stat DefenseStat
    {
        get
        {
            return Player.Instance.DefenseStat;
        }
    }

    public Stat CriticalChanceStat
    {
        get
        {
            return Player.Instance.CriticalChanceStat;
        }
    }

    public Stat CriticalValueStat
    {
        get
        {
            return Player.Instance.CriticalValueStat;
        }
    }

    public bool IsSpecialActive
    {
        get
        {
            return false;
        }
    }

    public string SpecialAttack
    {
        get
        {
            return "";
        }
    }

    #endregion

    private void OnCollisionEnter(Collision collision)
    {
        Combatant otherCombatant = collision.gameObject.GetComponent<Combatant>();
        if (otherCombatant != null)
            CombatResolution.PlayerVSCombatantCollision(this, otherCombatant, collision);
    }

}

