﻿using UnityEngine;
using System.Collections;

public class EnemyCombatant : MonoBehaviour, Combatant
{

    [SerializeField] private float health = 20;

    public GameObject GameObject
    {
        get
        {
            return gameObject;
        }
    }

    [SerializeField] private GameObject debrisPrefab;
    public GameObject DebrisPrefab
    {
        get
        {
            return debrisPrefab;
        }
    }

    [SerializeField] private float baseAttack = 3;
    public float BaseAttack
    {
        get
        {
            return baseAttack;
        }
    }
    [SerializeField] private float baseDefense = 3;
    public float BaseDefense
    {
        get
        {
            return baseDefense;
        }
    }

    public bool ApplyDamage(float damage)
    {
        health -= damage;
        Debug.LogFormat("{0} received {1} damage", name, damage);
        return health <= 0;
    }

    [SerializeField] private Stat attackStat;
    public Stat AttackStat
    {
        get
        {
            return attackStat;
        }
    }

    [SerializeField] private Stat defenseStat;
    public Stat DefenseStat
    {
        get
        {
            return defenseStat;
        }
    }

    [SerializeField] private Stat criticalChanceStat;
    public Stat CriticalChanceStat
    {
        get
        {
            return criticalChanceStat;
        }
    }

    [SerializeField] private Stat criticalValueStat;
    public Stat CriticalValueStat
    {
        get
        {
            return criticalChanceStat;
        }
    }

    public CombatantType CombatantType
    {
        get
        {
            return CombatantType.Enemy;
        }
    }
    public bool IsSpecialActive
    {
        get
        {
            return false;
        }
    }
    public string SpecialAttack
    {
        get
        {
            return "";
        }
    }
}

