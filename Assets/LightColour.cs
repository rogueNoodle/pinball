﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class LightColour : MonoBehaviour {

    [SerializeField] private Color consciousColor;
    [SerializeField] private Color unconsciousColor;
    private Light light;
    private void Awake ()
    {
        light = GetComponent<Light>();
        MessageDispatcher.AddListener(PlayerMessages.ConsciousStateChanged, OnConsciousStateChanged);
    }

    private void OnDestroy ()
    {
        MessageDispatcher.RemoveListener(PlayerMessages.ConsciousStateChanged, OnConsciousStateChanged);
    }

    private void OnConsciousStateChanged (IMessage message)
    {
        bool state = (bool)message.Data;
        light.color = state ? consciousColor : unconsciousColor;
    }
}
